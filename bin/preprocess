#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
File: preprocess.py
Author: Lukas Galke
Email: vim@lpag.de
Github: https://github.com/lgalke
Description: Convert csv tables into graph structure and preprocess textual features.
"""

import os
import pickle
import json

import numpy as np
import torch
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS

from qgraph.data import (PAPER_TYPE, BiblioGraph, NumericBiblioGraph,
                         load_tables)
from qgraph.preprocessing import AlphaNumericTextPreprocessor


def main(args):
    """Main entry point for preprocessing
    1. Preprocess graph
    2. Preprocess text

      :args: TODO
      :returns: TODO

    """
    tables = load_tables(args.tablesdir,
                         with_authors=(args.authors != 'ignore'),
                         # ignore_title=args.ignore_title,
                         low_memory=args.low_memory)

    bgraph = BiblioGraph.from_tables(*tables,
                                     supervised=False,
                                     undirected=True,
                                     collate_coauthorship=(args.authors == 'collate'),
                                     add_source=args.add_source)

    if args.thesaurus:
        print("Loading concept hierarchy from", args.thesaurus)
        with open(args.thesaurus, 'r') as jsonfile:
            thesaurus = json.load(jsonfile)
        bgraph.expand_with_thesaurus(thesaurus)

    # Do the critical step of assigned vertex ids to nodes!
    numeric_graph, ndata = bgraph.numericalize()

    # Align titles and vertex ids, very important
    paper_titles = bgraph.paper_features['title']

    # Only compare identifier to paper type, else is not necessarily unique
    paper_vids = ndata[ndata.type == PAPER_TYPE]\
        .join(paper_titles, on='identifier', how='inner')\
        .index.values

    if not args.ignore_title:
        print("Sample vid,titles")
        print(*zip(paper_vids[:5], paper_titles[:5]), sep='\n')

        if args.reuse_preprocessor:
            print("Restoring preprocessor from", args.preprocessor)
            with open(args.preprocessor, 'rb') as fhandle:
                preprocessor = pickle.load(fhandle)
            title_features = preprocessor.transform(paper_titles.values, return_lengths=True)
        else:
            print("Preprocessing text...")
            preprocessor = AlphaNumericTextPreprocessor(max_features=args.max_features,
                                                        lowercase=True,
                                                        max_length=args.max_length,
                                                        stop_words=ENGLISH_STOP_WORDS,
                                                        drop_unknown=True,
                                                        dtype=torch.tensor)
            title_features, lengths = preprocessor.fit_transform(paper_titles.values,
                                                                return_lengths=True)

            lengths = np.asarray(lengths)
            print("Length mean/std: {:.2f} +- {:.2f}".format(lengths.mean(), lengths.std()))

        # `title_features` defined

        # Get actually used max_length (could be None before)
        feature_dim = title_features.size(1)

        features = torch.zeros(bgraph.graph.number_of_nodes(), feature_dim,
                            dtype=title_features.dtype)
        features[paper_vids] = title_features

        print(' ==> Preprocessed paper features for sample')
        print(features[:5])
    else:
        features = None
        preprocessor = None
    nbg = NumericBiblioGraph(numeric_graph, ndata, features, preprocessor=preprocessor)
    print("Saving numeric bibliograph to", args.outdir)
    nbg.save(args.outdir)




if __name__ == "__main__":
    import argparse
    PARSER = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    PARSER.add_argument("tablesdir",
                        help="""Path to tables dir containing files
                        - 'paper.csv',
                        - 'annotation.csv',
                        - 'authorship.csv'""")
    PARSER.add_argument('--max-features', type=int, default=None,
                        help="Vocabulary size for words")
    PARSER.add_argument('--max-length', type=int, default=None,
                        help="Maximum word count of titles")
    PARSER.add_argument('--authors', default='include',
                        choices=['include', 'collate', 'ignore'],
                        help="Treatment of authors.")
    PARSER.add_argument('--thesaurus',
                        help="Path to thesaurus file injson format {'<concept>': ['<broader_concept_1>',...], ...}")
    PARSER.add_argument('--low-memory', action='store_true', default=False,
                        help="Use low memory settings for data loading")
    PARSER.add_argument('--reuse-preprocessor',
                        help="Reuse an existing pickled preprocessor (pickled)")
    PARSER.add_argument('--add-source', default=False, action='store_true',
                        help="Add sources (like the journal) of papers as nodes")
    PARSER.add_argument('-o', '--outdir', default=None,
                        help="Path to output dir")
    PARSER.add_argument('--ignore-title', default=False, action='store_true',
                        help="Ignore title column")
    ARGS = PARSER.parse_args()
    if ARGS.outdir is None:
        # Reasonable default output dir
        import hashlib
        h = hashlib.new("md5")
        s = bytes(str(ARGS), 'utf-8')
        h.update(s)
        ARGS.outdir = os.path.join("/tmp/qgraph", h.hexdigest())

    # Create dir and store params used for preprocessing
    os.makedirs(ARGS.outdir, exist_ok=True)
    with open(os.path.join(ARGS.outdir, 'preprocess_args.txt'), 'w') as fh:
        print(ARGS, file=fh)

    print("Output will be written to:", ARGS.outdir)
    main(ARGS)
