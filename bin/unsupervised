#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""
File: unsupervised
Author: Lukas Galke
Email: vim@lpag.de
Github: https://github.com/lgalke
Description: Script to run supervised experiments on bibliographic data
"""
import itertools as it
import logging
import os
import random
import pickle
from collections import defaultdict
import time

import dgl
import dgl.function as fn
import networkx as nx
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from dgl.contrib.sampling.sampler import NeighborSampler
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS
from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm
import scipy.sparse as sp

from qgraph.data import PAPER_TYPE, SUBJECT_TYPE, NumericBiblioGraph
from qgraph.embedding import MaskedNodeEmbedding
from qgraph.functions import compute_gcn_norm
from qgraph.models import DGI, GAE, GCN, GCNSampling, GraphSAGE
from qgraph.models.lsa import LSAEmbedding, class_prototypes
from qgraph.preprocessing import AlphaNumericTextPreprocessor
from qgraph.utils import save_embedding, dump_temporal_embeddings, load_embedding
from qgraph.train.embed_deepwalk import embed_deepwalk
from qgraph.train.train_gcn_cv_sc import train_gcn_cv_sc
from qgraph.train.train_dgi_cv_sc import train_dgi_cv_sc
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

DEBUG = False


# Model directory structure
MDS = {
    "embedding_file": "embedding.csv",
    "args_file": "args.txt",
    "loss_file": "losses.txt",
    "accuracy_file": "accuracy.txt",
    "temporal_embeddings_dir": "temporal_embeddings",
    "ccount_file": "cumulative_counts.csv",
    "checkpoints_dir": "checkpoints"
}

def embed_lsa(args, data):
    """ Embeds data via latent semantic analysis """

    features, preprocessor = data.features, data.preprocessor
    paper_vs = data.ndata[data.ndata.type == PAPER_TYPE].index.values
    paper_feats = features[paper_vs].numpy()
    n_samples = paper_feats.shape[0]


    # Convert sequential text features to bag-of-words
    # Add 1 for padding
    print("Converting sequential features to bag of words")
    S = sp.dok_matrix((n_samples, len(preprocessor.vocabulary_) + 1), dtype=np.int64)
    print(S)
    for i in tqdm(range(n_samples)):
        row = paper_feats[i]
        for token in row:
            if token != preprocessor.padding_idx_:
                S[i, token] += 1
    S = S.tocsr(copy=False)

    lsa = LSAEmbedding(n_components=args.representation_size)
    print("Applying TFIDF and SVD")
    x_papers = lsa.fit_transform(S)
    print("Explained variance {:.4%}".format(lsa.svd.explained_variance_ratio_.sum()))
    print("Creating class protoypes")
    y_labels = data.label_indicator_matrix()
    print("Edges between papers and concepts", y_labels.count_nonzero())
    subject_embedding = class_prototypes(x_papers, y_labels)
    subject_ids = data.ndata[data.ndata.type == SUBJECT_TYPE]["identifier"].values
    print("Done.")
    return subject_ids, subject_embedding

def embed_dgi(args, data):
    print("Converting to dgl graph...")
    t0 = time.time()
    g = dgl.DGLGraph(data.graph)
    print("Done, took", time.time() - t0, "seconds (wall clock time)")
    g.set_n_initializer(dgl.init.zero_initializer)
    print("Adding self loop")
    g.add_edges(g.nodes(), g.nodes())

    concept_vs = torch.LongTensor(data.ndata[data.ndata.type == SUBJECT_TYPE].index.values)


    concept_msk = torch.zeros(g.number_of_nodes())
    concept_msk[concept_vs] = 1.0

    features, preprocessor = data.features, data.preprocessor
    print("Text features size", features.size())

    n_concepts = concept_vs.size(0)
    print("N Concepts:", n_concepts)

    # We want embeddings in that dim, so we need to use them as hidden unit dim
    emb_size = args.embedding_dim
    n_hidden = args.representation_size


    use_sparse_encoder = not args.scale_grad_by_freq
    # + 1 for padding
    text_encoder = nn.Embedding(len(preprocessor.vocabulary_) + 1,
                                emb_size,
                                sparse=use_sparse_encoder, padding_idx=0,
                                max_norm=args.max_norm,
                                scale_grad_by_freq=args.scale_grad_by_freq)
    model = DGI(emb_size, n_hidden, activation=nn.PReLU(n_hidden))

    if args.use_cuda:
        text_encoder = text_encoder.cuda()
        model = model.cuda()
        features = features.cuda()
        concept_msk = concept_msk.cuda()
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")

    b_xent = nn.BCEWithLogitsLoss()
    if use_sparse_encoder:
        embedding_optimizer = optim.SparseAdam(text_encoder.parameters(),
                                               lr=args.lr)
    else:
        embedding_optimizer = optim.Adam(text_encoder.parameters(),
                                         lr=args.lr)
    model_optimizer = optim.Adam(model.parameters(),
                                 lr=args.lr,
                                 weight_decay=args.weight_decay)

    if args.early_stopping:
        # Init early stopping
        cnt_wait = 0
        best = 1e9
        best_t = 0
    losses = []
    for epoch in range(args.epochs):
        # Apply model
        features_emb = text_encoder(features).mean(1)
        # Corrupt graph by shuffling node features
        idx = np.random.permutation(g.number_of_nodes())
        shuf_fts_emb = features_emb[idx, :]

        # Construct labels
        lbl_1 = torch.ones(g.number_of_nodes(), 1, device=device)
        lbl_2 = torch.zeros(g.number_of_nodes(), 1, device=device)
        lbl = torch.cat((lbl_1, lbl_2), 1)
        # Code
        if DEBUG:
            print("Feats", features.size())
            print("Shufs", shuf_fts.size())

        h = model(features_emb, shuf_fts_emb, g, concept_msk, None, None)

        if DEBUG:
            print("h", h.size())
            print("lbl", lbl.size())

        # Compute loss
        loss = b_xent(h, lbl)

        if args.early_stopping:
            if loss < best:
                best = loss
                best_t = epoch
                cnt_wait = 0
                torch.save(text_encoder.state_dict(), os.path.join(args.out, 'best_text_encoder.pkl'))
                torch.save(model.state_dict(), os.path.join(args.out, 'best_model.pkl'))
            else:
                cnt_wait += 1

            if cnt_wait == args.patience:
                print("Early stopping!")
                # Break out of main training loop if early-stopping criterion is met
                break

        # Clear gradient
        embedding_optimizer.zero_grad()
        model_optimizer.zero_grad()

        # Compute gradient
        loss.backward()

        # Optimize params
        embedding_optimizer.step()
        model_optimizer.step()

        # Store loss for reporting
        print("Epoch {}: {:.4f}".format(epoch, loss.item()))

    if args.early_stopping:
        print('Loading {}th epoch'.format(best_t))
        text_encoder.load_state_dict(torch.load(os.path.join(args.out, 'best_text_encoder.pkl')))
        model.load_state_dict(torch.load(os.path.join(args.out, 'best_model.pkl')))
        # For logging purposes
        epoch = best_t

    print("Encoding once more to get the embedding")
    text_encoder.eval()
    model.eval()
    with torch.no_grad():
        features_emb = text_encoder(features).mean(1)
        concepts = data.ndata[data.ndata.type == SUBJECT_TYPE]["identifier"]
        node_ids, descriptors = concepts.index.values, concepts.values
        # descriptors = data.ndata[data.ndata.type == SUBJECT_TYPE]["identifier"].values

        if args.temporal:
            print("Dumping temporal embeddings...")
            dump_temporal_embeddings(os.path.join(args.out, MDS['temporal_embeddings_dir']),
                                     model.embed,
                                     g, features_emb,
                                     data.paper_features.index.values,
                                     data.paper_features.year.values,
                                     node_ids,
                                     descriptors,
                                     ccount_path=os.path.join(args.out, MDS['ccount_file']),
                                     min_year=args.start_year)
            print("Done.")

        # But papers get their embeddings
        embedding = model.embed(features_emb, g)
        subject_embedding = embedding[concept_vs]
    return descriptors, subject_embedding.cpu().numpy()


def embed_random(args, data):
    concepts = data.ndata[data.ndata.type == SUBJECT_TYPE]["identifier"]
    embedding = np.random.rand(len(concepts), args.representation_size)
    return concepts.values, embedding

def main(args):
    if args.use_cuda and not torch.cuda.is_available():
        print("Cuda not available, falling back to CPU")

    if args.temporal and not args.model in ['dgi', 'gcn_cv_sc', 'dgi_cv_sc', 'deepwalk']:
        print("Model", args.model, "is not capable of computing temporal embeddings")
        exit(1)

    args.use_cuda = args.use_cuda and torch.cuda.is_available()

    # some models don't need a preprocessor
    needs_features = args.model not in ['random', 'deepwalk']
    needs_preprocessor = args.model not in ['random', 'deepwalk']
    print("Loading data...")
    data = NumericBiblioGraph.load(args.graphdir, undirected=True,
                                   with_features=needs_features,
                                   with_preprocessor=needs_preprocessor)

    # Switch case on main training function
    labels, embedding = {
        'random': embed_random,
        'lsa': embed_lsa,
        'deepwalk': embed_deepwalk,
        'dgi': embed_dgi,
        'gcn_cv_sc': train_gcn_cv_sc,
        'dgi_cv_sc': train_dgi_cv_sc
    }[args.model](args, data)


    with open(os.path.join(args.out, MDS['args_file']), 'w') as argsfile:
        print(args, file=argsfile)


    embedding_file = os.path.join(args.out, MDS['embedding_file'])
    save_embedding(labels, embedding, embedding_file)


if __name__ == '__main__':
    import argparse
    top_parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # parent parser for shared arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, add_help=False)
    subparsers = top_parser.add_subparsers(dest='model',
    help="Select model for representation learning")
    # parser.add_argument('model',
    #                     choices=['lsa',
    #                              'gae',
    #                              'deepwalk',
    #                              'dgi',
    #                              'dgi_cv_sc',
    #                              'gcn_cv_sc',
    #                              'random'],
    #                     help="Select model for representation learning")
    parser.add_argument('graphdir',
                        help='path to graph dir')
    parser.add_argument('-s', '--representation-size', type=int, default=32,
                        help="Target embedding dimension")
    parser.add_argument('-o', '--out',
                        help='Output directory')
    parser.add_argument('--epochs', default=200, type=int,
                        help="Number of epochs")
    parser.add_argument('--no-cuda', dest='use_cuda', default=True,
                        action='store_false',
                        help='Do not use GPU processing!')

    parser.add_argument("--workers", default=4, type=int, help="CPU workers")

    dgi_parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, add_help=False)
    dgi_parser.add_argument('--lr', type=float, default=1e-3,
                            help="Learning rate")
    dgi_parser.add_argument('--weight-decay', type=float, default=0,
                            help="Weight decay")
    # Early stopping
    # When relying on the *training* loss instead of validation loss, we call this "aborting training" instead of early stopping
    dgi_parser.add_argument("--early-stopping", default=False, action='store_true', help="Use early stopping")

    # GCN specific
    gcn_parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, add_help=False, parents=[dgi_parser])
    gcn_args = gcn_parser.add_argument_group('GCN arguments')
    gcn_args.add_argument("--n-layers", default=2, type=int,
                        help="How many GCN layers to use")
    gcn_args.add_argument("--n-hidden", default=16, type=int,
                        help="How many GCN layers to use")
    gcn_args.add_argument("--dropout", default=0.5, type=float,
                        help="Dropout factor within GCN layers")
    gcn_args.add_argument("--decoder-bias", default=False, help="Enable bias in decoder",
                        action='store_true')
    gcn_args.add_argument("-f", "--fastmode", default=False, help="Only compute final accuracy.",
                        action='store_true')
    gcn_args.add_argument("--scale-grad-by-freq", default=False, help="Scale grad by batch IDF",
                        action='store_true')
    gcn_args.add_argument("--globals-on-cpu", default=False, help="Store globals on CPU memory.",
                        action='store_true')
    gcn_args.add_argument("--warm-start",
                        default=False,
                        action='store_true',
                        help="Restore model checkpoint in outdir for warm start")

    # Batching specific
    batch_args = parser.add_argument_group('batching arguments')
    batch_args.add_argument("--batch-size", default=32, type=int, help="Batch size for training")
    batch_args.add_argument("--test-batch-size", default=None, type=int, help="Batch size for testing")

    # Text Embedding specific
    embed_args = parser.add_argument_group('text embedding arguments')
    embed_args.add_argument('--embedding-dim', type=int, default=64,
        help="Text embedding dimension")
    embed_args.add_argument('--embedding-dropout', type=float, default=0,
        help="Text embedding dimension")
    embed_args.add_argument("--max-norm", default=None, help="Max norm for text embedding", type=float)

    # Sampling specific
    sampl_args = gcn_parser.add_argument_group('sampling')
    sampl_args.add_argument("--num-neighbors", default=2, type=int,
        help="How many neighbors to sample in each layer")

    tempo_parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, add_help=False)
    tempo_args = tempo_parser.add_argument_group('temporal arguments')
    tempo_args.add_argument("--temporal", default=False, action='store_true',
                        help="Store temporal embeddings")
    tempo_args.add_argument("--temporal-window", default=None, type=int,
                        help="Consider years [x - <temporal window size>, x] for each year x")
    tempo_args.add_argument("--start-year", type=int, default=None,
                        help="Start year for temporal embeddings")
    tempo_args.add_argument("--end-year", type=int, default=None,
                        help="End year for temporal embeddings")

    subparsers.add_parser('lsa', parents=[parser])
    # subparsers.add_parser('gae', parents=[parser])
    subparsers.add_parser('dgi', parents=[parser, dgi_parser, tempo_parser])

    # Deepwalk-specific params
    dw_parser = subparsers.add_parser('deepwalk', parents=[parser, tempo_parser])
    dw_args = dw_parser.add_argument_group('deepwalk specific')
    dw_args.add_argument("--number-walks", default=1, type=int, help="Number of walks per node in Deepwalk")
    dw_args.add_argument("--walk-length", default=3, type=int, help="Walk length in Deepwalk")
    dw_args.add_argument("--window-size", default=3, type=int, help="Window size for skip-gram in Deepwalk")
    dw_args.add_argument('--lr', type=float, default=1e-3, help="Learning rate")
    dw_args.add_argument("--abort-training", default=False, action='store_true', help="Use some sort of early stopping for deepwalk")
    dw_args.add_argument("--patience", default=20, type=int, help="Patience for early stopping (or arborting training when using deepwalk)")
    dw_args.add_argument("--negative", default=5, type=int, help="Number of negative samples for deepwalk")

    subparsers.add_parser('dgi_cv_sc', parents=[parser, gcn_parser, tempo_parser])
    subparsers.add_parser('gcn_cv_sc', parents=[parser, gcn_parser, tempo_parser])
    subparsers.add_parser('random', parents=[parser])

    # parser.add_argument("--thesaurus", default=None, help="Thesaurus")

    ARGS = top_parser.parse_args()

    # hacky workaround for weird behaviour if called without any arguments
    if ARGS.model is None:
        top_parser.parse_args(['--help'])

    ARGS.test_batch_size = ARGS.batch_size if ARGS.test_batch_size is None else ARGS.test_batch_size

    ARGS.MDS = MDS
    print(ARGS)

    if ARGS.temporal_window is not None:
        assert ARGS.temporal, "--temporal-window should only be used with --temporal"
        assert ARGS.model == "deepwalk", "Only deepwalk supports temporal window"

    if not ARGS.out:
        # Guess some tmp directory to put output
        ARGS.out = os.path.join("/tmp/",
                                os.path.basename(os.path.dirname(ARGS.graphdir)),
                                ARGS.model)

    print("Storing outputs to", ARGS.out)
    os.makedirs(ARGS.out, exist_ok=True)

    main(ARGS)
