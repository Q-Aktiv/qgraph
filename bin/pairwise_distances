#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Compute all pairwise distances within an embedding file,
optionally centered and normalized, no duplicates and no self distances."""
import argparse
import numpy as np
from sklearn.metrics import pairwise_distances
from qgraph.utils import load_embedding


def main():
    """ Main entry point """
    parser = argparse.ArgumentParser()
    parser.add_argument("embedding_csv_file",
                        help="Path to csv Embedding file")
    parser.add_argument("-m", "--metric", default="cosine",
                        help="Distance metric")
    parser.add_argument("--center",
                        help="Center the data points",
                        action="store_true", default=False)
    parser.add_argument("--normalize",
                        help="Normalize each feature of the data points",
                        action="store_true", default=False)
    parser.add_argument("-o", "--outfile", help="Path of output file")
    args = parser.parse_args()

    __concepts, embedding = load_embedding(args.embedding_csv_file,
                                           as_dataframe=False)

    if args.center:
        centroid = np.mean(embedding, axis=0)
        embedding = embedding - centroid

    if args.normalize:
        norm = np.linalg.norm(embedding, axis=0, keepdims=True)
        embedding = embedding / norm

    dists = pairwise_distances(embedding, embedding, metric=args.metric)

    # only get the values within the upper triagonal, without diagonal
    upper_ind = np.triu_indices(dists.shape[0], k=1)
    values = dists[upper_ind].reshape(-1)

    print("N = ", values.size)
    print("mean = ", values.mean())
    print("std (N-1) = ", values.std(ddof=1))

    if args.outfile:
        values.tofile(args.outfile, sep="\n")


if __name__ == "__main__":
    main()
