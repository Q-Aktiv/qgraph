#!/bin/bash

bin/unsupervised gcn_cv_sc "data/rel/zbmedke-food-pharma/"\
	--max-features 50000 --max-length 15 \
	--lr 0.001 --epochs 150000 --dropout 0.5 \
	--num-neighbors 100 --n-layers 1 --batch-size 128 \
	--embedding-dim 64 --n-hidden 64 -s 64 \
	--no-authors \
	--early-stopping --patience 20 \
	--temporal --fastmode -o "data/emb/ke-food-pharma-faster"


# usage: unsupervised [-h] [--min-count MIN_COUNT] [--max-features MAX_FEATURES]
#                     [--max-length MAX_LENGTH] [-s REPRESENTATION_SIZE]
#                     [--lr LR] [--weight-decay WEIGHT_DECAY] [-o OUT]
#                     [--epochs EPOCHS] [--no-cuda]
#                     [--number-walks NUMBER_WALKS] [--walk-length WALK_LENGTH]
#                     [--window-size WINDOW_SIZE] [--workers WORKERS]
#                     [--batch-size BATCH_SIZE]
#                     [--test-batch-size TEST_BATCH_SIZE]
#                     [--embedding-dim EMBEDDING_DIM]
#                     [--embedding-dropout EMBEDDING_DROPOUT]
#                     [--max-norm MAX_NORM] [--num-neighbors NUM_NEIGHBORS]
#                     [--n-layers N_LAYERS] [--n-hidden N_HIDDEN]
#                     [--dropout DROPOUT]
#                     [--representation-dropout REPRESENTATION_DROPOUT]
#                     [--representation-activation REPRESENTATION_ACTIVATION]
#                     [--representation-layer-norm] [--decoder-bias] [-f]
#                     [--scale-grad-by-freq] [--temporal] [--globals-on-cpu]
#                     [--first-class-authors]
#                     {lsa,gae,deepwalk,dgi,gcn_cv_sc,random} graphdir

# positional arguments:
#   {lsa,gae,deepwalk,dgi,gcn_cv_sc,random}
#                         Select model for representation learning
#   graphdir              path to graph dir

# optional arguments:
#   -h, --help            show this help message and exit
#   --min-count MIN_COUNT
#                         Minimum count for words (default: 5)
#   --max-features MAX_FEATURES
#                         Vocabulary size for words (default: 50000)
#   --max-length MAX_LENGTH
#                         Maximum word count of titles (default: None)
#   -s REPRESENTATION_SIZE, --representation-size REPRESENTATION_SIZE
#                         Target embedding dimension (default: 32)
#   --lr LR               Learning rate (default: 0.001)
#   --weight-decay WEIGHT_DECAY
#                         Weight decay (default: 0)
#   -o OUT, --out OUT     Output directory (default: None)
#   --epochs EPOCHS       Number of epochs (default: 200)
#   --no-cuda             Do not use GPU processing! (default: True)
#   --number-walks NUMBER_WALKS
#                         Number of walks per node in Deepwalk (default: 5)
#   --walk-length WALK_LENGTH
#                         Walk length in Deepwalk (default: 3)
#   --window-size WINDOW_SIZE
#                         Window size for skip-gram in Deepwalk (default: 3)
#   --workers WORKERS     CPU workers for Deepwalk (default: 4)
#   --batch-size BATCH_SIZE
#                         Batch size for training (default: 32)
#   --test-batch-size TEST_BATCH_SIZE
#                         Batch size for testing (default: None)
#   --embedding-dim EMBEDDING_DIM
#                         Text embedding dimension (default: 64)
#   --embedding-dropout EMBEDDING_DROPOUT
#                         Text embedding dimension (default: 0)
#   --max-norm MAX_NORM   Max norm for text embedding (default: None)
#   --num-neighbors NUM_NEIGHBORS
#                         How many neighbors to sample in each layer (default:
#                         2)
#   --n-layers N_LAYERS   How many GCN layers to use (default: 2)
#   --n-hidden N_HIDDEN   How many GCN layers to use (default: 16)
#   --dropout DROPOUT     Dropout factor within GCN layers (default: 0.5)
#   --representation-dropout REPRESENTATION_DROPOUT
#                         Apply dropout on the representation layer (default: 0)
#   --representation-activation REPRESENTATION_ACTIVATION
#                         Apply dropout on the representation layer (default:
#                         None)
#   --representation-layer-norm
#                         Apply layer norm on the representation layer (default:
#                         False)
#   --decoder-bias        Enable bias in decoder (default: False)
#   -f, --fastmode        Only compute final accuracy. (default: False)
#   --scale-grad-by-freq  Scale grad by batch IDF (default: False)
#   --temporal            Store temporal embeddings (default: False)
#   --globals-on-cpu      Store globals on CPU memory. (default: False)
#   --first-class-authors
#                         Do not collate authorship. (default: False)
