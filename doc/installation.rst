.. _installation:

Installation
============

Clone the qgraph repository and enter its directory.

.. code-block:: bash

  git clone https://gitlab.com/Q-Aktiv/qgraph.git
  cd qgraph

Set a python virtual environment up and activate it (recommended).

.. code-block:: bash

  virtualenv --python=python3 venv
  source venv/bin/activate  # Repeat this in each session

Install the package and all its requirements.

.. code-block:: bash

  pip install -r requirements.txt

