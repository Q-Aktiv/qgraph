.. _schema:


Schema
======


Entity-Relationship Diagram
---------------------------

.. figure:: _static/schema.png
   :scale: 50 %
   :alt: Relational schema for graph data



Relational Schema
-----------------

* Paper: (paper_id, year, title, source)
* Authorship: (paper_id, author)
* Annotation: (paper_id, subject)

