.. _executables:


Executables
===========

Our executables live in the `bin/` subdirectory.


Harvesting
----------

The goal of these two scripts is to harvest a jsonlines file and output csv tables according to our relational :ref:`schema`.

* `harvest_econbiz`
* `harvest_livivo`


Preprocessing
-------------

The `bin/preprocessing` script takes care of two steps:

* Creates a graph structure
* Creates a vocabulary for textual features

The output of the `bin/preprocessing` script is a fully numerical graph along with supplementary node data `ndata.csv` indexed by node id. The supplementary data includes the node type and the papers' year.


Representation Learning
-----------------------

The `bin/unsupervised` script is the main entry point for training node representation learning models.
It features:

* DeepWalk
* metapath2vec
* Graph Convolutional Networks
* Deep Graph Infomax
