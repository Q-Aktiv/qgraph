.. qgraph documentation master file, created by
   sphinx-quickstart on Thu Nov 29 00:27:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to qgraph's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   schema
   executables



The purpose of qgraph is to provide an experimental setup to conduct graph
analyses for the Q-Active project.




Notes
-----

* Use csv tables along with pandas to save and load the relations.
* Use networkx for graph construction as it offers valuable graph methods and
  has capabilities to output a sparse adjacency matrix.
* The networkx library supplies standard shortest-path algorithms along with
  in-degree and out-degree.
* We handle different node types by prefixing, e.g. :code:`"__PAPER__ "`, :code:`"__AUTHOR__ "`
* Graph embedding algorithms such as GCN/GAT algorithms can work with such
  sparse adjacency matrices to learn
  a node embedding.
* Use pyveplot for graph visualization?
* Networkx also supports extra attributes on the nodes.
* DGL provides key features to work with GCNs/GATs.
* DGL extra-attributes are limited to tensors.
* ZBMedKE has a 'MESH' field.
* We filter for 'eng' (English) on the 'LANGUAGE' field.
* We use a common embedding format in csv where the first column refers to the
  key and the remaining columns to the embedding dimensions.
* We can separate between supervised and unsupervised training
* STW: Economis -> 16 subconcepts, Business Economics -> 11 subconcepts, Economic Sectors -> 34 subconcepts, Commodities -> 20 subconcepts, Related subject areas -> 11 subconcepts, Geographic Names -> 7 subconcepts, General descriptors -> 2 subconcepts
* STW: #concepts on first level = 16 + 11 + 34 + 20 + 11 + 7 + 2 = 101


Stats
-----

.. code-block:: bash

  (venv) [kdsrv03 qgraph* reorganize]!2342 > wc -l data/raw/zbmedke_20190205.json 
  28185760 data/raw/zbmedke_20190205.json
  (venv) [kdsrv03 qgraph* reorganize]!2343 > cat data/raw/zbmedke_20190205.json | grep ARTICLELANGUAGE | wc -l
  6416388
  (venv) [kdsrv03 qgraph* reorganize]!2344 > cat data/raw/zbmedke_20190205.json | grep "\"LANGUAGE\"" | wc -l
  24759356
  (venv) [kdsrv03 qgraph* reorganize]!2345 > cat data/raw/zbmedke_20190205.json | grep "\"ARTICLELANGUAGE\"" | wc -l
  6416388




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
