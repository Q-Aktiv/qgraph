from setuptools import setup

setup(name='qgraph',
      version='0.1',
      description='Graph embedding techniques and induced similarity measures',
      url='http://gitlab.com/Q-Aktiv/qgraph',
      author='Lukas Galke',
      author_email='python@lpag.de',
      license='GPLv3',
      packages=['qgraph'],
      install_requires=[
          'numpy',
          'scipy>=1.1.0',
          'pandas',
          'networkx>=2.1',
          'tqdm',
          'jsonlines',
          'scikit-learn',
          'matplotlib',
          'seaborn',
          'torch',
          'dgl',
          'rdflib',
          'gensim'
      ],
      setup_requires=["pytest-runner"],
      tests_require=["pytest"],
      scripts=[
          'bin/harvest_livivo',   # Harvesting script for LIVIVO
          'bin/harvest_econbiz',  # Harvesting script for ECONBIZ
          'bin/unsupervised',
          'bin/supervised',
          'bin/plot_tsne',
          'bin/cluster',
          'bin/nearest_concepts',  # Find nearest concepts of input
          'bin/trace_concepts',  # Trace two concepts through time
          'bin/trace_concept_sets',  # Trace two sets of concepts through time
          'bin/most_common_concepts',
          'bin/list_unique_concepts',
          'bin/aggregate_concepts'  # Aggregate multiple line-concept files into single json
      ],
      zip_safe=False)


# vim: set sw=4:
