# qgraph

[![pipeline status](https://gitlab.com/Q-Aktiv/qgraph/badges/master/pipeline.svg)](https://gitlab.com/Q-Aktiv/qgraph/commits/master)
[![coverage report](https://gitlab.com/Q-Aktiv/qgraph/badges/master/coverage.svg)](https://gitlab.com/Q-Aktiv/qgraph/commits/master)

Graph embedding techniques and similarity measures.

## Installation

```sh
git clone git@gitlab.com:Q-Aktiv/qgraph.git
cd qgraph
virtualenv --python=python3 venv  # create python3 virtual environment
source venv/bin/activate  # repeat in each session
pip install -r requirements.txt
```

## Documentation

[Documentation](https://q-aktiv.gitlab.io/qgraph)
