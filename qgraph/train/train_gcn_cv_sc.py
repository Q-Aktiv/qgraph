import os
from collections import defaultdict

import dgl
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from dgl.contrib.sampling.sampler import NeighborSampler

from qgraph.models import DGI, GCN, GCNSampling
from qgraph.data import PAPER_TYPE, SUBJECT_TYPE, NumericBiblioGraph
from qgraph.utils import dump_temporal_embeddings


def train_gcn_cv_sc(args, data):
    """train_gcn_cv_sc
    Learns an embedding with GCNs with control variate sampling and skip-connections.
    Valid args.model keys are 'graphsage_cv' or 'gcn_cv_sc'.

    :param args: Namespace for command line arguments
    :param data: BiblioGraph instance
    """
    # Can go to command line args later
    num_neighbors = args.num_neighbors
    n_layers = args.n_layers
    dropout = args.dropout
    emb_size = args.embedding_dim
    n_hidden = args.n_hidden
    rep_size = args.representation_size

    device = torch.device("cuda") if args.use_cuda else torch.device("cpu")
    globals_device = device if not args.globals_on_cpu else torch.device("cpu")

    # Preparing lists of vertices
    subjects = data.ndata[data.ndata.type == SUBJECT_TYPE]
    subject_vs = subjects.index.values
    paper_vs = data.ndata[data.ndata.type == PAPER_TYPE].index.values
    print(subject_vs)
    print(len(subject_vs))

    print("Nxgraph")
    print("Number of nodes:", data.graph.number_of_nodes())
    print("Number of edges:", data.graph.number_of_edges())


    print("Creating DGL graph ...")
    g = dgl.DGLGraph(data.graph, readonly=True)
    g.set_n_initializer(dgl.init.zero_initializer)
    print("DGL Graph")
    print("Number of nodes:", g.number_of_nodes())
    print("Number of edges:", g.number_of_edges())
    print("Is multigraph:", g.is_multigraph)
    print("Setting zero initializer")
    g.set_n_initializer(dgl.init.zero_initializer)

    ### INIT 'features'
    features, preprocessor = data.features, data.preprocessor
    print("Feats size", features.size())
    print("Adding features")
    g.ndata['features'] = features.to(globals_device)

    print("Mapping subject nids to class label")
    print("Subj values", subject_vs)
    subject2classlabel = defaultdict(lambda: -1, {nid:c for c, nid in enumerate(subject_vs)})
    # print("Map", subject2classlabel)
    n_classes = len(subject2classlabel)
    targets = torch.zeros(g.number_of_nodes(), dtype=torch.int64) - 1
    # -1 as default since paper nodes shouldnt be subject to optim target anyways
    for nid in subject_vs:
        targets[nid] = subject2classlabel[nid]

    targets = targets.to(device)

    print("Subject targets:", targets[subject_vs])
    print("Number of classes", n_classes)

    ### INIT 'norm' (both gcn/graphsage use this norm)
    print("Computing global norm...")
    norm = 1./g.in_degrees().float().unsqueeze(1)
    norm[torch.isinf(norm)] = 0.  # Prevent INF values
    # print("Spurious rows", data.ndata[norm.numpy() == 0.])
    print("Norm", norm, sep='\n')
    print("Norm size", norm.size())
    g.ndata['norm'] = norm.to(globals_device)

    text_encoder = nn.Embedding(len(preprocessor.vocabulary_) + 1,
                                emb_size,
                                sparse=False, padding_idx=0,
                                max_norm=args.max_norm,
                                scale_grad_by_freq=args.scale_grad_by_freq)

    if args.embedding_dropout:
        text_encoder = nn.Sequential(text_encoder, nn.Dropout(args.embedding_dropout))

    model = GCNSampling(emb_size,
                        n_hidden,
                        rep_size,
                        n_layers,
                        F.relu,
                        dropout)

    # Linear decoder
    decoder = nn.Linear(rep_size, n_classes, bias=args.decoder_bias)

    if args.warm_start:
        print("Loading model checkpoint from", args.out)
        text_encoder.load_state_dict(torch.load(os.path.join(args.out, 'best_text_encoder.pkl')))
        model.load_state_dict(torch.load(os.path.join(args.out, 'best_model.pkl')))
        decoder.load_state_dict(torch.load(os.path.join(args.out, 'best_decoder.pkl')))

    # Keep large embedding on CPU when globals are on CPU
    text_encoder = text_encoder.to(globals_device)
    model = model.to(device)
    decoder = decoder.to(device)

    model.init_globals(g, device=globals_device)

    loss_fcn = nn.CrossEntropyLoss()

    embed_optimizer = optim.Adam(text_encoder.parameters(), lr=args.lr)
    model_optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    decoder_optimizer = optim.Adam(decoder.parameters(), lr=args.lr, weight_decay=args.weight_decay)

    print(text_encoder)
    print(model)
    print(decoder)

    ############
    # Training #
    ############

    def validate(text_encoder, model, decoder, g, subject_vs, targets):
        # Closure, uses args of outer scope
        num_acc = 0.
        val_loss = 0.
        text_encoder.eval()
        model.eval()
        decoder.eval()
        # Evaluate accuracy
        for nf in dgl.contrib.sampling.NeighborSampler(g, args.test_batch_size,
                                                       g.number_of_nodes(),  # expand factor
                                                       neighbor_type='in',
                                                       num_hops=n_layers,
                                                       seed_nodes=subject_vs,
                                                       add_self_loop=False,
                                                       num_workers=args.workers,
                                                       prefetch=True):
            # Copy data from global graph
            node_embed_names = [['features']]
            for i in range(n_layers):
                node_embed_names.append(['norm'])

            nf.copy_from_parent(node_embed_names=node_embed_names)

            with torch.no_grad():
                nf.apply_layer(0, lambda node:
                               {'embed': text_encoder(node.data['features']).mean(1)})
                embedded_feats = nf.layers[0].data['embed']
                z = model(embedded_feats, nf)
                pred = decoder(z)
                batch_nids = nf.layer_parent_nid(-1)
                batch_targets = targets[batch_nids]
                loss = loss_fcn(pred, batch_targets)
                num_acc += (torch.argmax(pred, dim=1) == batch_targets).sum().item()
                val_loss += loss.detach().item() * nf.layer_size(-1)

        accuracy = num_acc / len(subjects)
        val_loss = val_loss / len(subjects)

        return val_loss, accuracy

    losses = []
    step = 0
    if args.early_stopping:
        # Init early stopping
        cnt_wait = 0
        best = 1e9
        best_t = 0

    for epoch in range(args.epochs):
        # Make sure everything is in train mode!!!
        text_encoder.train()
        model.train()
        decoder.train()
        epoch_loss = 0.
        for nf in dgl.contrib.sampling.NeighborSampler(g, args.batch_size,
                                                       num_neighbors,
                                                       neighbor_type='in',
                                                       shuffle=True,
                                                       num_hops=n_layers,
                                                       add_self_loop=False,
                                                       seed_nodes=subject_vs,
                                                       num_workers=args.workers,
                                                       prefetch=True):
            step += 1
            # Fill aggregate history from neighbors
            model.aggregate_history(g, nf)

            # Copy data from parent
            node_embed_names = model.names_from_parent()
            node_embed_names[0].append('features')
            nf.copy_from_parent(node_embed_names=node_embed_names)

            # forward
            model_optimizer.zero_grad()
            embed_optimizer.zero_grad()
            decoder_optimizer.zero_grad()

            # First, apply the text encoder
            # nf.layers[0].data['features'] = text_encoder(nf.layers[0].data['features'])
            nf.apply_layer(0, lambda node: {'embed':
                                            text_encoder(node.data['features']).mean(1)})
            embedded_feats = nf.layers[0].data['embed']
            z = model(embedded_feats, nf)
            pred = decoder(z)

            batch_nids = nf.layer_parent_nid(-1)
            batch_targets = targets[batch_nids]
            loss = loss_fcn(pred, batch_targets)
            # backward
            loss.backward()

            model_optimizer.step()
            embed_optimizer.step()
            decoder_optimizer.step()

            nf.copy_to_parent(node_embed_names=model.names_to_parent())

            # Loss is sample-averaged, for epoch loss, we need to multiply it
            # back in and divide later
            epoch_loss += loss.detach().item() * nf.layer_size(-1)
            # print("Step {:7d} | Epoch {:4d} | Loss {:.4f}".format(step, epoch+1, loss.item()))


        avg_epoch_loss = epoch_loss / len(subjects)
        # Now expand to all nodes for getting final represenations
        with open(os.path.join(args.out, args.MDS['loss_file']), 'a') as lossfile:
            print("{:.4f}".format(avg_epoch_loss), file=lossfile)


        if args.early_stopping:
            if avg_epoch_loss < best:
                best = avg_epoch_loss
                best_t = epoch
                cnt_wait = 0
                torch.save(text_encoder.state_dict(), os.path.join(args.out, 'best_text_encoder.pkl'))
                torch.save(model.state_dict(), os.path.join(args.out, 'best_model.pkl'))
                torch.save(decoder.state_dict(), os.path.join(args.out, 'best_decoder.pkl'))
            else:
                cnt_wait += 1

            if cnt_wait == args.patience:
                print("Early stopping!")
                # Break out of main training loop if early-stopping criterion is met
                break

        if args.fastmode:
            # In fast mode, only evaluate accuracy in final epoch!
            print("Step {:7d} | Epoch {:4d} | Train Loss: {:.4f}".format(step,
                                                                         epoch,
                                                                         avg_epoch_loss))
            # Skip per-epoch computation of validation accuracy/loss
            continue

        val_loss, accuracy = validate(text_encoder, model, decoder, g, subject_vs, targets)
        with open(os.path.join(args.out, args.MDS['accuracy_file']), 'a') as accfile:
            print("Step {:7d} | Epoch {:4d} | Train loss: {:.4f} | Eval loss: {:.4f} | Accuracy {:.4f}"
                  .format(step, epoch, avg_epoch_loss, val_loss, accuracy), file=accfile)
        print("Step {:7d} | Epoch {:4d} | Train Loss: {:.4f} | Eval loss: {:.4f} | Accuracy {:.4f}"
              .format(step, epoch, avg_epoch_loss, val_loss, accuracy))

    if args.early_stopping:
        print('Loading {}th epoch'.format(best_t))
        text_encoder.load_state_dict(torch.load(os.path.join(args.out, 'best_text_encoder.pkl')))
        model.load_state_dict(torch.load(os.path.join(args.out, 'best_model.pkl')))
        decoder.load_state_dict(torch.load(os.path.join(args.out, 'best_decoder.pkl')))
        # For logging purposes
        epoch = best_t



    print("Shift models on cpu...")
    # PUT EVERYTHING STILL NEEDED ON CPU
    # WE DONT WANT TO RUN INTO MEM ISSUES HERE
    text_encoder = text_encoder.cpu()
    model = model.cpu()
    decoder = decoder.cpu()

    g.ndata['features'] = g.ndata['features'].cpu()
    g.ndata['norm'] = g.ndata['norm'].cpu()
    targets = targets.cpu()

    for i in range(n_layers):
        g.ndata.pop('h_{}'.format(i))
        g.ndata.pop('agg_h_{}'.format(i))

    # Put stuff in eval mode, no dropout and stuff
    text_encoder.eval()
    model.eval()
    decoder.eval()

    # and comp accuracy on the fly
    try:
        print("Computing final decoding accuracy")
        val_loss, accuracy = validate(text_encoder, model, decoder, g, subject_vs, targets)
        with open(os.path.join(args.out, args.MDS['accuracy_file']), 'a') as accfile:
            print("Step {:7d} | Epoch {:4d} | Eval loss: {:.4f} | Accuracy {:.4f}"
                  .format(step, epoch, val_loss, accuracy), file=accfile)
        print("Step {:7d} | Epoch {:4d} | Eval loss: {:.4f} | Accuracy {:.4f}"
              .format(step, epoch, val_loss, accuracy))
    except RuntimeError as e:
        print(e)
        print("Could not compute final decoding accuracy... continuing")


    # Preprocess text encoding
    # Save representation
    def embedding_fn(features, graph, node_ids):
        # Closure to compute embedding from subgraph
        embedding = torch.zeros(graph.number_of_nodes(), rep_size)
        model.eval()
        graph.ndata['embed'] = features
        norm = 1./graph.in_degrees().float().unsqueeze(1)
        norm[torch.isinf(norm)] = 0.  # Prevent INF values
        graph.ndata['norm'] = norm

        # For now, we only do it for subject_vs
        for nf in dgl.contrib.sampling.NeighborSampler(graph, args.test_batch_size,
                                                       graph.number_of_nodes(),  #expand factor
                                                       neighbor_type='in',
                                                       num_hops=n_layers,
                                                       seed_nodes=node_ids,
                                                       num_workers=args.workers,
                                                       prefetch=True,
                                                       add_self_loop=True):  # Self-loop required for temporal stuff
            # Copy data from global graph
            node_embed_names = [['embed']]
            for i in range(n_layers):
                node_embed_names.append(['norm'])
            nf.copy_from_parent(node_embed_names=node_embed_names)
            with torch.no_grad():
                z = model(nf.layers[0].data['embed'], nf)
                embedding[nf.layer_parent_nid(-1)] = z
        # Cleanup
        graph.ndata.pop('embed')
        graph.ndata.pop('norm')
        return embedding[node_ids]

    text_encoder.eval()
    features_emb = text_encoder(g.ndata.pop('features')).mean(1)

    print("features for final repr (after embedding):", features_emb, sep='\n')

    concepts = data.ndata[data.ndata.type == SUBJECT_TYPE]["identifier"]
    concept_nids, descriptors = concepts.index.values, concepts.values


    # Snapshot embeddings
    if args.temporal:
        print("g just before temporal embeddings (after dropping features):", g, sep='\n')
        paper_year_df = data.ndata[data.ndata.type == PAPER_TYPE]["year"]
        paper_nids, paper_years = paper_year_df.index.values, paper_year_df.values

        print("Dumping temporal embeddings...")
        dump_temporal_embeddings(os.path.join(args.out, args.MDS['temporal_embeddings_dir']),
                                 embedding_fn,
                                 g, features_emb,
                                 paper_nids,
                                 paper_years,
                                 concept_nids,
                                 descriptors,
                                 ccount_path=os.path.join(args.out, args.MDS['ccount_file']),
                                 min_year=args.start_year)
        print("Done.")

    print("Computing global embeddings...")
    # Global embedding
    representation = embedding_fn(features_emb, g, concept_nids).numpy()
    print("Done.")
    return descriptors, representation
