import os
import time

import dgl
import dgl.function as fn
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from dgl.contrib.sampling.sampler import NeighborSampler

from qgraph.data import PAPER_TYPE, SUBJECT_TYPE, NumericBiblioGraph
from qgraph.models import DGI, GCNSampling
from qgraph.utils import dump_temporal_embeddings


def train_dgi_cv_sc(args, data):
    """train_dgi_cv_sc
    Learns an embedding with GCNs with control variate sampling and skip-connections.
    Valid args.model keys are 'graphsage_cv' or 'gcn_cv_sc'.

    :param args: Namespace for command line arguments
    :param data: BiblioGraph instance
    """
    # Can go to command line args later
    num_neighbors = args.num_neighbors
    n_layers = args.n_layers
    dropout = args.dropout
    emb_size = args.embedding_dim
    n_hidden = args.n_hidden
    rep_size = args.representation_size

    device = torch.device("cuda") if args.use_cuda else torch.device("cpu")
    globals_device = device if not args.globals_on_cpu else torch.device("cpu")

    # Preparing lists of vertices
    subjects = data.ndata[data.ndata.type == SUBJECT_TYPE]
    subject_vs = subjects.index.values
    paper_vs = data.ndata[data.ndata.type == PAPER_TYPE].index.values
    print(subject_vs)
    print(len(subject_vs))

    print("Nxgraph")
    print("Number of nodes:", data.graph.number_of_nodes())
    print("Number of edges:", data.graph.number_of_edges())
    features, preprocessor = data.features, data.preprocessor
    print("Text feature dims", features.size())

    print("Creating DGL graph ...")
    t0 = time.time()
    g = dgl.DGLGraph(data.graph, readonly=True)
    print("Done, took", time.time() - t0, "seconds (wall clock time)")

    print("DGL Graph")
    print("Number of nodes:", g.number_of_nodes())
    print("Number of edges:", g.number_of_edges())

    print("Is multigraph:", g.is_multigraph)
    print("Setting zero initializer")
    g.set_n_initializer(dgl.init.zero_initializer)

    ### INIT 'features'
    print("Adding features")
    g.ndata['features'] = features.to(globals_device)

    ### INIT 'norm' (both gcn/graphsage use this norm)
    print("Computing global norm...")
    norm = 1./g.in_degrees().float().unsqueeze(1)
    norm[torch.isinf(norm)] = 0.  # Prevent INF values
    # print("Spurious rows", data.ndata[norm.numpy() == 0.])
    print("Norm", norm, sep='\n')
    print("Norm size", norm.size())
    g.ndata['norm'] = norm.to(globals_device)

    text_encoder = nn.Embedding(len(preprocessor.vocabulary_) + 1,
                                emb_size,
                                sparse=False, padding_idx=0,
                                max_norm=args.max_norm,
                                scale_grad_by_freq=args.scale_grad_by_freq)

    if args.embedding_dropout:
        text_encoder = nn.Sequential(text_encoder, nn.Dropout(args.embedding_dropout))

    gcn = GCNSampling(emb_size,
                      n_hidden,
                      rep_size,
                      n_layers,
                      nn.LeakyReLU(),
                      dropout)
    model = DGI(emb_size, rep_size, gcn=gcn)

    if args.warm_start:
        print("Loading model checkpoint from", args.out)
        text_encoder.load_state_dict(torch.load(os.path.join(args.out, 'best_text_encoder.pkl')))
        model.load_state_dict(torch.load(os.path.join(args.out, 'best_model.pkl')))

    text_encoder = text_encoder.to(device)
    model = model.to(device)

    gcn.init_globals(g, prefix='true', device=globals_device)
    gcn.init_globals(g, prefix='noise', device=globals_device)

    b_xent = nn.BCEWithLogitsLoss()

    embed_optimizer = optim.Adam(text_encoder.parameters(), lr=args.lr)
    model_optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)

    print(text_encoder)
    print(model)

    ############
    # Training #
    ############
    def corrupt(features):
        n = features.size(0)
        idx = np.random.permutation(n)
        shuf_fts = features[idx, :]
        return shuf_fts

    def validate(text_encoder, model, g, subject_vs):
        # Closure, uses args of outer scope
        val_loss = 0.
        text_encoder.eval()
        model.eval()
        # Evaluate accuracy
        for nf in dgl.contrib.sampling.NeighborSampler(g, args.test_batch_size,
                                                       g.number_of_nodes(),  # expand factor
                                                       neighbor_type='in',
                                                       num_hops=n_layers,
                                                       seed_nodes=subject_vs,
                                                       add_self_loop=False,
                                                       num_workers=args.workers,
                                                       prefetch=True):
            # Copy data from global graph
            node_embed_names = [['features']]
            for i in range(n_layers):
                node_embed_names.append(['norm'])

            nf.copy_from_parent(node_embed_names=node_embed_names)

            with torch.no_grad():
                nf.apply_layer(0, lambda node:
                               {'embed': text_encoder(node.data['features']).mean(1)})
                embedded_feats = nf.layers[0].data['embed']
                shuffled_feats = corrupt(embedded_feats)
                device = embedded_feats.device
                lbl_1 = torch.ones(nf.layer_size(-1), 1, device=device)
                lbl_2 = torch.zeros(nf.layer_size(-1), 1, device=device)
                labels = torch.cat((lbl_1, lbl_2), 1)
                logits = model(embedded_feats, shuffled_feats, nf, prefix1='true', prefix2='noise')
                loss = b_xent(logits, labels)
                val_loss += loss.detach().item() * nf.layer_size(-1)

        val_loss = val_loss / len(subjects)

        return val_loss

    losses = []
    step = 0
    if args.early_stopping:
        # Init early stopping
        cnt_wait = 0
        best = 1e9
        best_t = 0

    for epoch in range(args.epochs):
        # Make sure everything is in train mode!!!
        text_encoder.train()
        model.train()
        epoch_loss = 0.
        for nf in dgl.contrib.sampling.NeighborSampler(g, args.batch_size,
                                                       num_neighbors,
                                                       neighbor_type='in',
                                                       shuffle=True,
                                                       num_hops=n_layers,
                                                       add_self_loop=False,
                                                       seed_nodes=subject_vs,
                                                       num_workers=args.workers,
                                                       prefetch=True):
            step += 1
            # Fill aggregate history from neighbors
            gcn.aggregate_history(g, nf, prefix='true')
            gcn.aggregate_history(g, nf, prefix='noise')

            # Copy features
            node_embed_names = [['features']]
            for i in range(n_layers):
                node_embed_names.append([])
            nf.copy_from_parent(node_embed_names=node_embed_names)

            # Copy globals from parent graph to nodeflow
            # Put both prefixed variants together
            # [[str]] of length n_layers + 1
            node_embed_names = [t+n for t, n in zip(gcn.names_from_parent('true'),
                                                   gcn.names_from_parent('noise'))]
            # make sure input feats are also included
            node_embed_names[0].append('features')
            nf.copy_from_parent(node_embed_names=node_embed_names)

            # forward
            model_optimizer.zero_grad()
            embed_optimizer.zero_grad()

            # First, apply the text encoder
            # nf.layers[0].data['features'] = text_encoder(nf.layers[0].data['features'])
            nf.apply_layer(0, lambda node: {'embed': text_encoder(node.data['features']).mean(1)})

            embedded_feats = nf.layers[0].data['embed']
            shuffled_feats = corrupt(embedded_feats)

            # Final layer size for true/false pseudo labels
            lbl_1 = torch.ones(nf.layer_size(-1), 1, device=device)
            lbl_2 = torch.zeros(nf.layer_size(-1), 1, device=device)
            labels = torch.cat((lbl_1, lbl_2), 1)

            logits = model(embedded_feats, shuffled_feats, nf, prefix1='true', prefix2='noise')
            loss = b_xent(logits, labels)

            # backward
            loss.backward()

            model_optimizer.step()
            embed_optimizer.step()

            node_embed_names = [t+n for t, n in zip(gcn.names_to_parent('true'),
                                                   gcn.names_to_parent('noise'))]
            nf.copy_to_parent(node_embed_names=node_embed_names)

            # Loss is sample-averaged, for epoch loss, we need to multiply it
            # back in and divide later
            epoch_loss += loss.detach().item() * nf.layer_size(-1)
            # print("Step {:7d} | Epoch {:4d} | Loss {:.4f}".format(step, epoch+1, loss.item()))


        avg_epoch_loss = epoch_loss / len(subjects)
        # Now expand to all nodes for getting final represenations
        with open(os.path.join(args.out, args.MDS['loss_file']), 'a') as lossfile:
            print("{:.4f}".format(avg_epoch_loss), file=lossfile)


        if args.early_stopping:
            if avg_epoch_loss < best:
                best = avg_epoch_loss
                best_t = epoch
                cnt_wait = 0
                torch.save(text_encoder.state_dict(), os.path.join(args.out, 'best_text_encoder.pkl'))
                torch.save(model.state_dict(), os.path.join(args.out, 'best_model.pkl'))
            else:
                cnt_wait += 1

            if cnt_wait == args.patience:
                print("Early stopping!")
                # Break out of main training loop if early-stopping criterion is met
                break

        if args.fastmode:
            # In fast mode, only evaluate accuracy in final epoch!
            print("Step {:7d} | Epoch {:4d} | Train Loss: {:.4f}".format(step,
                                                                         epoch,
                                                                         avg_epoch_loss))
            # Skip per-epoch computation of validation accuracy/loss
            continue

        val_loss = validate(text_encoder, model, g, subject_vs)
        with open(os.path.join(args.out, args.MDS['accuracy_file']), 'a') as accfile:
            print("Step {:7d} | Epoch {:4d} | Train loss: {:.4f} | Eval loss: {:.4f}"
                  .format(step, epoch, avg_epoch_loss, val_loss), file=accfile)
        print("Step {:7d} | Epoch {:4d} | Train Loss: {:.4f} | Eval loss: {:.4f}"
              .format(step, epoch, avg_epoch_loss, val_loss))

    if args.early_stopping:
        print('Loading {}th epoch'.format(best_t))
        text_encoder.load_state_dict(torch.load(os.path.join(args.out, 'best_text_encoder.pkl')))
        model.load_state_dict(torch.load(os.path.join(args.out, 'best_model.pkl')))
        # For logging purposes
        epoch = best_t



    print("Shift models on cpu...")
    # PUT EVERYTHING STILL NEEDED ON CPU
    # WE DONT WANT TO RUN INTO MEM ISSUES HERE
    text_encoder = text_encoder.cpu()
    model = model.cpu()

    g.ndata['features'] = g.ndata['features'].cpu()
    g.ndata['norm'] = g.ndata['norm'].cpu()

    # Need specialized names for 'true' and 'noise'
    # for i in range(n_layers):
    #     g.ndata.pop('h_{}'.format(i))
    #     g.ndata.pop('agg_h_{}'.format(i))

    # Put stuff in eval mode, no dropout and stuff
    text_encoder.eval()
    model.eval()

    # and comp accuracy on the fly
    try:
        print("Computing final validation loss")
        val_loss = validate(text_encoder, model, g, subject_vs)
        with open(os.path.join(args.out, args.MDS['accuracy_file']), 'a') as accfile:
            print("Step {:7d} | Epoch {:4d} | Eval loss: {:.4f}"
                  .format(step, epoch, val_loss), file=accfile)
        print("Step {:7d} | Epoch {:4d} | Eval loss: {:.4f}"
              .format(step, epoch, val_loss))
    except RuntimeError as e:
        print(e)
        print("Could not compute final validation loss... continuing")



    # Preprocess text encoding
    # Save representation
    def embedding_fn(features, graph, node_ids):
        # Closure to compute embedding from subgraph
        embedding = torch.zeros(graph.number_of_nodes(), rep_size)
        model.eval()
        graph.ndata['embed'] = features
        norm = 1./graph.in_degrees().float().unsqueeze(1)
        norm[torch.isinf(norm)] = 0.  # Prevent INF values
        graph.ndata['norm'] = norm

        # For now, we only do it for subject_vs
        for nf in dgl.contrib.sampling.NeighborSampler(graph, args.test_batch_size,
                                                       graph.number_of_nodes(),  #expand factor
                                                       neighbor_type='in',
                                                       num_hops=n_layers,
                                                       seed_nodes=node_ids,
                                                       num_workers=args.workers,
                                                       prefetch=True,
                                                       add_self_loop=True):  # Self-loop required for temporal stuff
            # Copy data from global graph
            node_embed_names = [['embed']]
            for i in range(n_layers):
                node_embed_names.append(['norm'])
            nf.copy_from_parent(node_embed_names=node_embed_names)
            embedded_feats = nf.layers[0].data['embed']
            with torch.no_grad():
                z = model.embed(embedded_feats, nf)
                embedding[nf.layer_parent_nid(-1)] = z
        # Cleanup
        graph.ndata.pop('embed')
        graph.ndata.pop('norm')
        return embedding[node_ids]



    concepts = data.ndata[data.ndata.type == SUBJECT_TYPE]["identifier"]
    concept_nids, descriptors = concepts.index.values, concepts.values


    features_emb = text_encoder(g.ndata.pop('features')).mean(1)
    # Snapshot embeddings
    if args.temporal:
        print("g just before temporal embeddings (after dropping features):", g, sep='\n')
        paper_year_df = data.ndata[data.ndata.type == PAPER_TYPE]["year"]
        paper_nids, paper_years = paper_year_df.index.values, paper_year_df.values

        print("Dumping temporal embeddings...")
        dump_temporal_embeddings(os.path.join(args.out, args.MDS['temporal_embeddings_dir']),
                                 embedding_fn,
                                 g, features_emb,
                                 paper_nids,
                                 paper_years,
                                 concept_nids,
                                 descriptors,
                                 ccount_path=os.path.join(args.out, args.MDS['ccount_file']),
                                 min_year=args.start_year)
        print("Done.")

    print("Computing global embeddings...")
    # Global embedding
    representation = embedding_fn(features_emb, g, concept_nids).numpy()
    print("Done.")
    return descriptors, representation
