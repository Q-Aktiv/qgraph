import os
import random

from gensim.models import Word2Vec, KeyedVectors
import numpy as np

from qgraph.data import PAPER_TYPE, SUBJECT_TYPE, NumericBiblioGraph
from qgraph.models.deepwalk import graph as deepwalk_graph
from qgraph.utils import save_embedding

def embed_deepwalk_temporal(args, data, dw_graph, concept_node_ids, model):
    # Build index etc
    concepts = data.ndata[data.ndata.type == SUBJECT_TYPE]["identifier"]
    concept_node_ids, concept_descriptors = concepts.index.values, concepts.values
    concept_node_id_strings = concept_node_ids.astype('U') # used to access keyed vectors of model
    # Prepare years from data
    paper_year_df = data.ndata[data.ndata.type == PAPER_TYPE]["year"]
    paper_node_ids, paper_years = paper_year_df.index.values, paper_year_df.values

    outdir = os.path.join(args.out, args.MDS['temporal_embeddings_dir'])
    os.makedirs(outdir, exist_ok=True)

    if args.abort_training:
        best_embedding_file = os.path.join(args.out, 'best.embedding')

    has_valid_year = ~np.isnan(paper_years)
    print("%d of %d papers have a valid year" % (has_valid_year.sum(), paper_years.shape[0]))
    paper_node_ids, paper_years = paper_node_ids[has_valid_year], paper_years[has_valid_year]

    non_paper_node_ids = data.ndata[data.ndata.type != PAPER_TYPE].index.values

    all_years = np.unique(paper_years)  # np unique also sorts

    if args.start_year is not None:
        all_years = all_years[all_years >= args.start_year]
    if args.end_year is not None:
        all_years = all_years[all_years <= args.end_year]
    print("Considering all years between:", all_years[0], "and", all_years[-1])

    total_examples_per_epoch = len(concept_node_ids) * args.number_walks
    for year in all_years:
        print("Year", int(year))
        current_paper_nid = list(paper_node_ids[(paper_years > (year - args.temporal_window)) & (paper_years <= year)])
        print("N Papers", len(current_paper_nid))

        subg_nodes = list(non_paper_node_ids) + list(current_paper_nid)

        # Use set for faster lookup
        subg = dw_graph.subgraph(subg_nodes)
        print("N subgraph nodes", subg.number_of_nodes())
        print("N subgraph edges", subg.number_of_edges())

        outfile_year = os.path.join(outdir, str(int(year)) + '.csv')

        if args.abort_training:
            cnt_wait = 0
            best = 1e9
            best_t = 0

        # walk
        for epoch in range(args.epochs):
            walks_iter = deepwalk_graph.build_deepwalk_corpus_iter(subg,
                                                                   num_paths=args.number_walks,
                                                                   path_length=args.walk_length,
                                                                   alpha=0,
                                                                   rand=random.Random(epoch),
                                                                   seed_nodes=concept_node_ids)

            # Force string types as a generator
            walks_iter = ([str(node) for node in walk] for walk in walks_iter)

            # train
            model.train(walks_iter, epochs=1, total_examples=total_examples_per_epoch,
                        compute_loss=True)

            if args.abort_training:
                loss = model.get_latest_training_loss() / total_examples_per_epoch
                print("Epoch {}: {}".format(epoch, loss))
                if loss < best:
                    best = loss
                    best_t = epoch
                    cnt_wait = 0

                    # save intermediate embedding as gensim keyed vectors (pickle internally)
                    model.wv.save(best_embedding_file)
                else:
                    cnt_wait += 1

                if cnt_wait == args.patience:
                    print("Aborting training!")
                    # Break out of inner loop (training loop) and continue with next year
                    break

        if args.abort_training:
            print("Loading {}th epoch".format(best_t))
            model.wv = KeyedVectors.load(best_embedding_file)
            # For potential logging purposes
            epoch = best_t

        concept_embedding = model.wv[concept_node_id_strings]

        # save embedding for year as CSV
        save_embedding(concept_descriptors, concept_embedding, outfile_year)
        print("Loss after year {}: {}".format(year, model.get_latest_training_loss()))

def embed_deepwalk_non_temporal(args, data, dw_graph, concept_node_ids, model):
    """ Train DeepWalk `model` on `data` """

    print("N Papers", len(dw_graph.nodes()), "and in total")
    print("N Nodes", dw_graph.number_of_nodes(), "and")
    print("N Edges", dw_graph.number_of_edges())
    if args.abort_training:
        best_embedding_file = os.path.join(args.out, 'best.embedding')

    total_examples_per_epoch = len(concept_node_ids) * args.number_walks

    if args.abort_training:
        cnt_wait = 0
        best = 1e9
        best_t = 0

    # walk
    for epoch in range(args.epochs):
        walks_iter = deepwalk_graph.build_deepwalk_corpus_iter(dw_graph,
                                                               num_paths=args.number_walks,
                                                               path_length=args.walk_length,
                                                               alpha=0,
                                                               rand=random.Random(args.seed),
                                                               seed_nodes=concept_node_ids)

        # Force string types as a generator
        walks_iter = ([str(node) for node in walk] for walk in walks_iter)

        # train
        model.train(walks_iter, epochs=1, total_examples=total_examples_per_epoch,
                    compute_loss=True)

        if args.abort_training:
            loss = model.get_latest_training_loss() / total_examples_per_epoch
            print("Epoch {}, loss; {} ".format(epoch, loss))
            if loss < best:
                best = loss
                best_t = epoch
                cnt_wait = 0

                model.wv.save(best_embedding_file)
            else:
                cnt_wait += 1

            if cnt_wait == args.patience:
                print("Aborting training!")
                break

    if args.abort_training:
        print("Loading {}th epoch".format(best_t))

        model.wv = KeyedVectors.load(best_embedding_file)

        epoch = best_t

def embed_deepwalk(args, data):

    # BEGIN QND
    # TODO make this program args
    args.seed = 'seed'
    # END QND

    # Build graph
    g = data.graph
    outdir = args.out

    dw_graph = deepwalk_graph.from_networkx(g, undirected=True)
    print("dw_graph N nodes", dw_graph.number_of_nodes())
    print("dw_graph N edges", dw_graph.number_of_edges())

    os.makedirs(outdir, exist_ok=True)

    # Build index etc
    concepts = data.ndata[data.ndata.type == SUBJECT_TYPE]["identifier"]
    concept_node_ids, concept_descriptors = concepts.index.values, concepts.values
    concept_node_id_strings = concept_node_ids.astype('U') # used to access keyed vectors of model

    # Get all non-paper-nodes such that they are prevalent in every subgraph
    non_paper_node_ids = data.ndata[data.ndata.type != PAPER_TYPE].index.values

    # Set up model
    model = Word2Vec(size=args.representation_size,
                     window=args.window_size,
                     min_count=1,
                     sg=1,
                     hs=0,
                     negative=args.negative,
                     workers=args.workers,
                     sorted_vocab=0,
                     compute_loss=True,
                     alpha=args.lr,  # default 0.025
                     min_alpha=args.lr,
                     sample=0)


    # Build vocab
    print("Building vocabulary from graph ...")
    model.build_vocab([[str(n) for n in g.nodes()]], min_count=1)

    print("Concepts:", concept_node_ids)

    if args.temporal:
        embed_deepwalk_temporal(args, data, dw_graph, concept_node_ids, model)
    else:
        embed_deepwalk_non_temporal(args, data, dw_graph, concept_node_ids, model)

    final_embedding = model.wv[concept_node_id_strings]

    return concept_descriptors, final_embedding
