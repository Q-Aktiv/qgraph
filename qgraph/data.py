# Built-in imports
import os
import pickle
import itertools as it
# Third-party imports
from tqdm import tqdm
import pandas as pd
import json
import networkx as nx
import dgl as dgl
import numpy as np

import torch
from collections import namedtuple, deque


# We define a typed node such that key is garanteed to be unique within type
TypedNode = namedtuple('TypedNode', ['type', 'key'])

AUTHOR_TYPE = 'author'
PAPER_TYPE = 'paper'
SUBJECT_TYPE = 'subject'
VENUE_TYPE = 'venue'


def load_tables(graph_dir, with_authors=True, low_memory=False):
    """ Load tables as extracted by harvesting scripts """
    print("Loading graph from tables in:", graph_dir)
    print("Loading papers...")
    # if ignore_title:
    #     cols = ['paper_id', 'year', 'source']
    # else:
    #     cols = ['paper_id', 'year', 'source', 'title']
    df_paper = pd.read_csv(os.path.join(graph_dir, 'paper.csv'),
                           low_memory=low_memory)
    assert 'year' in df_paper.columns
    print("N papers", len(df_paper))
    print("Setting idx to first column")
    # Cast to string first
    df_paper[df_paper.columns[0]] = df_paper[df_paper.columns[0]].astype(str)
    # Set index inplace and drop old column
    # Important to access column 0 by its name
    df_paper.set_index(df_paper.columns[0], drop=True, inplace=True)
    print("Index type:", df_paper.index.dtype)
    print("Example index values:", df_paper.index[:5], sep='\n')
    print("Loading annotations...")
    df_annotation = pd.read_csv(os.path.join(graph_dir, 'annotation.csv'),
                                dtype={
                                    'paper_id': df_paper.index.dtype,
                                    'subject': str,
                                }, low_memory=low_memory)
    print("N annotations", len(df_annotation))
    if not with_authors:
        return df_paper, df_annotation

    # Else, also load authors
    print("Loading authors...")
    df_author = pd.read_csv(os.path.join(graph_dir, 'authorship.csv'),
                            dtype={'paper_id': df_paper.index.dtype, 'author': str},
                            usecols=['paper_id', 'author'],
                            low_memory=low_memory)
    print(df_author.head())

    print("N authorships", len(df_author))
    return df_paper, df_annotation, df_author

def load_thesaurus(thesaurustype, file_path):
    """ Load concept hierarchy, e. g. as extracted by harvesting script 'harvest_mesh' """
    # Deprecated: this method was merged into 'BiblioGraph.from_tables'
    # MESH and STW format were unified such that type switch-cases are not necessary anymore
    if thesaurustype not in ['mesh', 'stw'] or file_path is None:
        return None
    print("Loading " + thesaurustype + " concept hierarchy from:", file_path)
    with open(file_path, "r") as json_file:
        thesaurus = json.load(json_file)
    if thesaurustype == 'mesh':
        return thesaurus
    else: # thesaurustype must be 'stw' at this point
        vocabulary = { id: thesaurus[id]['prefLabel'][0] for id in thesaurus }
        return {
            vocabulary[id]: list(map(lambda x: vocabulary[x], thesaurus[id]['broader'])) for id in thesaurus
        }

class BiblioGraph():
    def __init__(self, graph, paper_features, paper_labels=None):
        """
        graph: nx.DiGraph
        paper_features: pandas.DataFrame
        """
        # Store the graph itself
        self.graph = graph
        # Paper has additional info we need to store
        self.paper_features = paper_features
        self.paper_labels = paper_labels  # None if unsupervised
        self.is_supervised = paper_labels is not None

    def __str__(self):
        return "BiblioGraph with {} nodes ({} papers) and {} edges."\
            .format(self.graph.number_of_nodes(),
                    len(self.paper_features),
                    self.graph.number_of_edges())


    # def save_cache(self, cache_dir):
    #     """ Saves a numerical representation of a graph to some cache"""
    #     if not self.is_numeric:
    #         raise ValueError("No need to save if not numeric")
    #     if self.is_supervised:
    #         raise NotImplementedError("Caching not yet implemented for supervised views")

    #     os.makedirs(cache_dir, exist_ok=True)
    #     # Adjacencies
    #     adj_path = os.path.join(cache_dir, "adjlist.txt")
    #     nx.readwrite.adjlist.write_adjlist(self.graph, adj_path)
    #     ndata_path = os.path.join(cache_dir, "ndata.csv")
    #     self.ndata.to_csv(ndata_path, index=True)

    # @staticmethod
    # def load_cache(cache_dir, paper_features_path, low_memory=False):
    #     """ Load a cached, numerical representation of a graph """
    #     # Adjacencies
    #     adj_path = os.path.join(cache_dir, "adjlist.txt")
    #     g = nx.readwrite.adjlist.read_adjlist(adj_path, nodetype=int)
    #     paper_features = pd.read_csv(paper_features_path, low_memory=low_memory)
    #     # Index are real identifiers
    #     paper_features.set_index(paper_features[paper_features.columns[0]].astype(str),
    #                              inplace=True)
    #     bg = BiblioGraph(g, paper_features)

    #     # Node data including types and stuff
    #     ndata = pd.read_csv(os.path.join(cache_dir, "ndata.csv"), index_col=0,
    #                         dtype={"identifier": str,
    #                                "type": str},
    #                         low_memory=low_memory)

    #     print("Ndata identifier dtype:", ndata.identifier.dtype)
    #     print("Paper features index dtype:", paper_features.index.dtype)

    #     reindexed_paper_features = ndata[ndata.type == PAPER_TYPE].join(paper_features,
    #                                                                     on="identifier",
    #                                                                     how="inner")
    #     print("Reindex paper features index dtype:", reindexed_paper_features.index.dtype)
    #     bg.ndata = ndata
    #     bg.paper_features = reindexed_paper_features
    #     bg.is_numeric = True
    #     bg.is_supervised = False
    #     bg.paper_labels = None
    #     return bg

    @staticmethod
    def from_tables(df_paper, df_annotation, df_author=None,
                    undirected=True,
                    supervised=False,
                    add_source=False,
                    collate_coauthorship=False):
        """from_tables

        :param df_paper: pandas.Dataframe with index being paper ids
        :param df_annotation: pandas.Dataframe with columns: paper ids, author ids
        :param df_author: pandas.Dataframe with columns: paper ids, author ids
        :param thesaurus: Dict mapping from each subject id to a list of its broader concepts' ids
        :param supervised: Don't add subject nodes but store label annotations separately
        :param add_venues: Add df_paper.venue nodes and respective connections to papers
        :param collate_coauthorship: Don't add author nodes, use co-authorship as edges
        """
        # Use undirected constructor right-away
        graph_cls = nx.Graph if undirected else nx.DiGraph
        g = graph_cls()

        if add_source:
            assert "source" in df_paper, "Sources requested but no 'source' column in dataframe"

        print("Adding paper nodes")
        for row in tqdm(df_paper.itertuples(index=True)):
            paper = TypedNode(PAPER_TYPE, row.Index)
            g.add_node(paper, year=row.year)

            if add_source:
                source = TypedNode(VENUE_TYPE, row.source)
                g.add_edge(paper, source)

        if df_author is not None:
            if not collate_coauthorship:
                print("Inserting authorship edges")
                for paper_id, author_id in tqdm(df_author.itertuples(index=False)):
                    paper = TypedNode(PAPER_TYPE, paper_id)
                    author = TypedNode(AUTHOR_TYPE, author_id)
                    g.add_edge(author, paper)
            else:
                print("Collating coauthorship edges between papers")
                # Col 1 must be the 'author' column
                for __author, group in tqdm(df_author.groupby('author', sort=False)):
                    coauthored_papers = group['paper_id'].values
                    for p1, p2 in it.product(coauthored_papers, coauthored_papers):
                        p1, p2 = TypedNode(PAPER_TYPE, p1), TypedNode(PAPER_TYPE, p2)
                        # Add edges between all edges of same author, including self edges
                        g.add_edge(p1, p2)


        print("Inserting annotation edges")
        for paper_id, subject_id in tqdm(df_annotation.itertuples(index=False)):
            paper = TypedNode(PAPER_TYPE, paper_id)
            subject = TypedNode(SUBJECT_TYPE, subject_id)
            g.add_edge(paper, subject)

        return BiblioGraph(g, df_paper)

    def expand_with_thesaurus(self, thesaurus):
        """ For concept nodes in graph recursively add all parent concepts """
        print("Expanding with thesaurus")
        old_n = self.graph.number_of_nodes()
        old_e = self.graph.number_of_edges()
        queue = deque([node for node in self.graph.nodes() if node.type == SUBJECT_TYPE])
        visited = set(queue)

        # Queue holds TypedNode instances!
        while queue:
            current = queue.popleft()
            try:
                broader = [TypedNode(SUBJECT_TYPE, c) for c in thesaurus[current.key]]
            except KeyError:
                print("Not found in thesaurus:", current.key)
                continue

            for parent in broader:
                self.graph.add_edge(current, parent)
                if parent not in visited:
                    visited.add(parent)
                    queue.append(parent)

        n_diff = self.graph.number_of_nodes() - old_n
        e_diff = self.graph.number_of_edges() - old_e
        print("Added {} new nodes and {} new edges".format(n_diff, e_diff))

    def numericalize(self):
        """ Converts nodes to integers """
        print("Converting node labels to integers")
        g = nx.convert_node_labels_to_integers(self.graph, label_attribute='_label')
        # >>> g = nx.Graph([(0,1),(2,3),(1,3),(2,4),(0,0)])
        # >>> g
       # <networkx.classes.graph.Graph object at 0x7f2d35121e80>
        # >>> g.nodes()
        # NodeView((0, 1, 2, 3, 4))
        # >>> g = nx.Graph([(3,1),(2,3),(1,3),(2,4),(0,0)])
        # >>> g.nodes()
        # NodeView((0, 1, 2, 3, 4))
        # >>> g = nx.Graph([('c','a'),('b','a'),('b','b'),('x','y'),('n','k')])
        # >>> g.nodes()
        # NodeView(('k', 'c', 'n', 'b', 'a', 'x', 'y'))
        # >>> gint = nx.convert_node_labels_to_integers(g)
        # >>> gint
        # <networkx.classes.graph.Graph object at 0x7f2d35121fd0>
        # >>> gint.nodes()
        # NodeView((0, 1, 2, 3, 4, 5, 6))
        print("Iterating graph to find node attributes")
        node_index, node_types, node_identifiers, node_years = [], [], [], []
        for node, node_data in g.nodes(data=True):
            node_type, node_identifier = node_data['_label']
            node_types.append(node_type)
            node_identifiers.append(node_identifier)
            node_index.append(node)
            year = node_data['year'] if 'year' in node_data else None
            node_years.append(year)


        # ndata is indexed by node_id, ordering is implied by g.nodes()
        ndata = pd.DataFrame({"type": node_types,
                              "identifier": node_identifiers,
                              "year": node_years},
                             index=node_index)

        return g, ndata




class NumericBiblioGraph():
    def __init__(self, graph, ndata, features=None, preprocessor=None):
        self.graph = graph
        self.ndata = ndata
        self.features = features
        self.preprocessor = preprocessor

    def __str__(self):
        n_authors = len(self.ndata[self.ndata.type == AUTHOR_TYPE])
        n_papers = len(self.ndata[self.ndata.type == PAPER_TYPE])
        n_subjects = len(self.ndata[self.ndata.type == SUBJECT_TYPE])
        avg_degree = np.mean(list(zip(*self.graph.degree))[1])
        return """
BiblioGraph with {} nodes ({} authors, {} papers, {} subjects)
and {} edges, average degree: {}."""\
            .format(self.graph.number_of_nodes(),
                    n_authors,
                    n_papers,
                    n_subjects,
                    self.graph.number_of_edges(),
                    avg_degree)

    def label_indicator_matrix(self, dtype=np.uint8):
        """ Return the label indicator matrix from papers to subjects.
        Paper i is annotated with subject j iff a[i][j] == 1"""
        # Method used in script bin/count_pairwise_concepts
        y = nx.convert_matrix.to_scipy_sparse_matrix(self.graph,
                                                     dtype=dtype,
                                                     weight=None,  # Put ones
                                                     format='csc')
        subject_vs = self.ndata[self.ndata.type == SUBJECT_TYPE].index.values
        paper_vs = self.ndata[self.ndata.type == PAPER_TYPE].index.values

        # Only consider subject subgraph
        y = y[:, subject_vs].tocsr()
        y = y[paper_vs, :]
        return y

    def save(self, path):
        os.makedirs(path, exist_ok=True)


        # Adjacencies
        adj_path = os.path.join(path, "adjlist.txt")
        nx.readwrite.adjlist.write_adjlist(self.graph, adj_path)
        # Additional data
        ndata_path = os.path.join(path, "ndata.csv")
        self.ndata.to_csv(ndata_path, index=True)

        if self.features is not None:
            assert isinstance(self.features, torch.Tensor)
            assert self.features.size(0) == self.graph.number_of_nodes()
            # Features
            feats_path = os.path.join(path, 'node_features.pt')
            torch.save(self.features, feats_path)

        if self.preprocessor is not None:
            with open(os.path.join(path, 'preprocessor.pkl'), 'wb') as fhandle:
                pickle.dump(self.preprocessor, fhandle)

    @staticmethod
    def load(path, low_memory=False, undirected=False,
             with_features=True,
             with_preprocessor=True):
        adj_path = os.path.join(path, "adjlist.txt")
        graph_cls = nx.Graph if undirected else nx.DiGraph
        Gnx = nx.readwrite.adjlist.read_adjlist(adj_path, create_using=graph_cls,
                                                nodetype=int)  # <-- Important!!
        ndata = pd.read_csv(os.path.join(path, "ndata.csv"),
                            index_col=0,
                            dtype={"identifier": str,
                                   "type": str,
                                   "year": float},
                            low_memory=low_memory)
        if with_features:
            feats_path = os.path.join(path, 'node_features.pt')
            features = torch.load(feats_path)
        else:
            features = None

        if with_preprocessor:
            with open(os.path.join(path, 'preprocessor.pkl'), 'rb') as fhandle:
                print("Restoring preprocessor from", fhandle.name)
                preprocessor = pickle.load(fhandle)
        else:
            preprocessor = None

        return NumericBiblioGraph(Gnx, ndata, features, preprocessor=preprocessor)
