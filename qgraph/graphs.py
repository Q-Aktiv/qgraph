"""
File: graphs.py
Author: Lukas Galke
Email: python@lpag.de
Github: https://github.com/lgalke
Description: Module for graphs
"""

#################################
### THIS MODULE IS DEPRECATED ###
#################################
raise DeprecationWarning("The graphs module is not used anymore. Data loading resides in .data instead.")


import os
import numpy as np
import scipy.sparse as sp
import pandas as pd
import networkx as nx

import itertools

from tqdm import tqdm


from collections import OrderedDict







def load_graph_from_tables(rootdir, directed=True):
    print("Loading graph from csv")
    df_paper = pd.read_csv(os.path.join(rootdir, 'paper.csv'), index_col=0)
    df_annotation = pd.read_csv(os.path.join(rootdir, 'annotation'),
                                dtype={'keyword': str, 'paper_id': str})
    df_author = pd.read_csv(os.path.join(rootdir, 'author.csv'),
                            dtype={'author': str, 'paper_id': str})


    # Prefixing is important to avoid identifier clashes
    print("Prefixing vertices")
    papers = [PAPER_PREFIX + str(p) for p in df_paper.index.get_values()]
    authors = [AUTHOR_PREFIX + str(a) for a in df_author['author'].unique()]
    keywords = [KEYWORD_PREFIX + str(kw) for kw in df_annotation['keyword'].unique()]

    nodes = authors + papers + keywords

    print("Prefixing edges")

    author_paper_edges = list(zip(
        df_author['author'].apply(lambda a: AUTHOR_PREFIX + str(a)).get_values(),
        df_author['paper_id'].apply(lambda p: PAPER_PREFIX + str(p)).get_values()
    ))

    paper_keyword_edges = list(zip(
        df_annotation['paper_id'].apply(lambda p: PAPER_PREFIX + str(p)).get_values(),
        df_annotation['keyword'].apply(lambda k: KEYWORD_PREFIX + str(k)).get_values()
    ))

    edges = author_paper_edges + paper_keyword_edges

    if directed:
        print("Constructing directed graph")
        G = nx.Graph()
    else:
        print("Constructing undirected graph")
        G = nx.DiGraph()

    G.add_nodes_from(tqdm(nodes))
    G.add_edges_from(tqdm(edges))

    return G


def adjacency_matrix(n_nodes, edges, dtype=bool, directed=True):
    """ Constructs an adjacency matrix

    :n_nodes: total number of nodes
    :edges: iterator of (src,dst)-pairs, where src,dst<n_nodes
    :dtype: desired data type (int will count duplicate edges)
    :directed: If not directed, the symmetric connections are respected
    """
    # use coo_matrix constructor instead?
    adj = sp.lil_matrix((n_nodes, n_nodes), dtype=dtype)
    for edge in edges:
        src, dst = edge
        adj[src, dst] += 1
        if not directed:
            adj[dst, src] += 1

    return adj


class Graph(object):
    """
    A class for graphs.

    >>> authors = ['mikolov', 'ben', 'vaswani']
    >>> papers  = ['word2vec',  'first-publication', 'transformer']
    >>> venues = ['nips', 'iclr']
    >>> has_written = [('mikolov', 'word2vec'), ('ben', 'first-publication'), ('vaswani', 'transformer')]
    >>> is_published = [('word2vec', 'nips'), ('word2vec','nips')]
    >>> g = Graph(authors+papers+venues, has_written+is_published, directed=True)
    >>> len(g) == len(authors) + len(papers) + len(venues)
    True
    >>> g.build_adjacency_matrix(dtype=int).toarray()
    array([[0, 0, 0, 1, 0, 0, 0, 0],
           [0, 0, 0, 0, 1, 0, 0, 0],
           [0, 0, 0, 0, 0, 1, 0, 0],
           [0, 0, 0, 0, 0, 0, 2, 0],
           [0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0]])
    >>> g.build_adjacency_matrix(dtype=bool).toarray()
    array([[False, False, False,  True, False, False, False, False],
           [False, False, False, False,  True, False, False, False],
           [False, False, False, False, False,  True, False, False],
           [False, False, False, False, False, False,  True, False],
           [False, False, False, False, False, False, False, False],
           [False, False, False, False, False, False, False, False],
           [False, False, False, False, False, False, False, False],
           [False, False, False, False, False, False, False, False]])
    >>> g.directed = False
    >>> g.build_adjacency_matrix(dtype=bool).toarray()
    array([[False, False, False,  True, False, False, False, False],
           [False, False, False, False,  True, False, False, False],
           [False, False, False, False, False,  True, False, False],
           [ True, False, False, False, False, False,  True, False],
           [False,  True, False, False, False, False, False, False],
           [False, False,  True, False, False, False, False, False],
           [False, False, False,  True, False, False, False, False],
           [False, False, False, False, False, False, False, False]])
    """
    def __init__(self, vertices, edges, directed=True):
        """Initializes a graph with a set of nodes and edges

        :V: array-like
        :E: scipy-sparse adjacency matrix
        :vocabs: List of (node_type: str , vocab: dict) tuples,
        """
        self.directed = directed
        self.vertices = vertices
        self.edges = edges
        self.vocab_ = {node: node_idx for node_idx, node
                       in enumerate(self.vertices)}

    def __len__(self):
        return len(self.vertices)

    def __str__(self):
        return "{} graph with {} vertices and {} edges".format("Directed" if self.directed else "Undirected",
                                                               len(self.vertices),
                                                               len(self.edges))

    def to_graphviz(self, filehandle):

        def escape(x):
            return '"'+x+'"'

        if self.directed:
            print("digraph G {", file=filehandle)
            edge_op = '->'
        else:
            print("graph G {", file=filehandle) 
            edge_op = '--'

        for v in self.vertices:
            print(escape(v) + ';', file=filehandle)
        for edge in self.edges:
            src, dst = edge
            print("{} {} {};".format(escape(src), edge_op, escape(dst)), file=filehandle)

        print("}", file=filehandle)




    def build_adjacency_matrix(self, dtype=int):
        # Make this a stand-alone function?
        """ Builds an adjacency matrix A with respect to vocabulary """
        edges_ix = ((self.vocab_[src], self.vocab_[dst])
                    for (src, dst) in self.edges)
        return adjacency_matrix(len(self.vertices),
                                edges_ix,
                                directed=self.directed,
                                dtype=dtype)




if __name__ == "__main__":
    import doctest
    doctest.testmod()
