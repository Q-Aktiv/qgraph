"""
File: embedding.py
Author: Lukas Galke
Email: vim@lpag.de
Github: https://github.com/lgalke
Description: Module for embedding bibliographic data
"""
import torch
import torch.nn as nn

class MaskedNodeEmbedding(nn.Module):
    """ Embed a subset of node """
    def __init__(self,
                 g,
                 mask,
                 embedding_dim,
                 max_norm=None,
                 norm_type=2.,
                 scale_grad_by_freq=False,
                 sparse=False,
                 _weight=None):
        super(MaskedNodeEmbedding, self).__init__()
        # If no mask is specified, embed all nodes
        num_nodes = g.number_of_nodes()
        # Make sure mask is a proper mask to index torch tensors.
        mask = torch.ByteTensor(mask)
        # Put one more vector, and reserve 0 as padding index
        num_embeddings, padding_idx = sum(mask) + 1, 0
        # index map, such that index_map[node_index] = emb_index
        index_map = torch.zeros(num_nodes).long()
        # Start with 1, such that 0 is padding
        index_map[mask] = torch.arange(1, num_embeddings)

        self.embedding = nn.Embedding(num_embeddings,
                                      embedding_dim,
                                      padding_idx=padding_idx,
                                      max_norm=max_norm,
                                      norm_type=norm_type,
                                      scale_grad_by_freq=scale_grad_by_freq,
                                      sparse=sparse,
                                      _weight=_weight
                                      )
        self.index_map = index_map

    def forward(self, g):
        # G may be a subgraph here, 
        nodes = g.nodes()
        nodes = self.index_map[nodes]
        node_embedding = self.embedding(nodes)
        return node_embedding



