# Edge Softmax 
# from .gat import GAT
from .gcn import GCN, GAE
from .mlp import MLP
from .gcn_cv_sc import GCNSampling
from .graphsage_cv import GraphSAGE
from .dgi import DGI
