"""GCN using DGL nn package

Adapted from: https://github.com/dmlc/dgl/blob/master/examples/pytorch/gcn/gcn.py

References:
- Semi-Supervised Classification with Graph Convolutional Networks
- Paper: https://arxiv.org/abs/1609.02907
- Code: https://github.com/tkipf/gcn
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
from dgl.nn.pytorch import GraphConv

class GCN(nn.Module):
    """ Standard graph conv net """
    def __init__(self,
                 g,
                 in_feats,
                 n_hidden,
                 n_classes,
                 n_layers,
                 activation,
                 dropout):
        super(GCN, self).__init__()
        self.g = g
        self.layers = nn.ModuleList()
        # input layer
        self.layers.append(GraphConv(in_feats, n_hidden, activation=activation))
        # hidden layers
        for i in range(n_layers - 1):
            self.layers.append(GraphConv(n_hidden, n_hidden, activation=activation))
        # output layer
        self.layers.append(GraphConv(n_hidden, n_classes))
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, features):
        h = features
        for i, layer in enumerate(self.layers):
            if i != 0:
                h = self.dropout(h)
            h = layer(h, self.g)
        return h

    def transform(self, features):
        h = features
        for i, layer in enumerate(self.layers):
            if i != 0:
                h = self.dropout(h)
            if i < len(self.layers) - 1:
                # Omit ultimate layer
                h = layer(h, self.g)
        return h


class GAE(nn.Module):
    def __init__(self, in_feats, n_hidden, n_layers, activation, dropout):
        """GAE
        :g: DGLGraph instance
        :in_feats: Input features
        :hidden_dim: Hidden dimension
        :n_layers: Number of encoder layers
        :activation: Activation function
        :dropout: dropout probability
        """
        super(GAE, self).__init__()
        # Code dim is given through number of nodes, since decoder is outer product
        self.encoder = nn.ModuleList()
        # self.encoder = GCN(g, in_feats, n_hidden, code_dim, n_layers, activation, dropout)
        # input layer
        self.encoder.append(GraphConv(in_feats, n_hidden, activation=activation))
        # hidden layers
        for __i in range(n_layers - 1):
            self.encoder.append(GraphConv(n_hidden, n_hidden, activation=activation))
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, features, g):
        h = features
        for i, layer in enumerate(self.encoder):
            if i != 0:
                h = self.dropout(h)
            h = layer(h, g)
        return h


    def decode(self, h):
        a_hat = h @ h.t()
        return torch.sigmoid(a_hat)
