""" Deep Graph Infomax
Based on author's implementation: https://github.com/PetarV-/DGI/
"""
DEBUG = False

# Torch imports
import torch
import torch.nn as nn

# DGL
from dgl.nn.pytorch import GraphConv

# Applies an average on seq, of shape (nodes, features)
# While taking into account the masking of msk
class AvgReadout(nn.Module):
    """ Default readout function """
    def __init__(self):
        super(AvgReadout, self).__init__()

    def forward(self, seq, msk):
        if msk is None:
            return torch.mean(seq, 0)
        else:
            msk = torch.unsqueeze(msk, -1)
            return torch.sum(seq * msk, 0) / torch.sum(msk)


class Discriminator(nn.Module):
    """ Discriminator """
    def __init__(self, n_h):
        super(Discriminator, self).__init__()
        self.f_k = nn.Bilinear(n_h, n_h, 1)

        for m in self.modules():
            self.weights_init(m)

    def weights_init(self, m):
        if isinstance(m, nn.Bilinear):
            torch.nn.init.xavier_uniform_(m.weight.data)
            if m.bias is not None:
                m.bias.data.fill_(0.0)

    def forward(self, c, h_pl, h_mi, s_bias1=None, s_bias2=None):
        if DEBUG:
            print("c", c.size())
            print("h_pl", h_pl.size())
            print("h_mi", h_mi.size())
        c_x = torch.unsqueeze(c, 0)
        c_x = c_x.expand_as(h_pl)

        sc_1 = self.f_k(h_pl, c_x)
        sc_2 = self.f_k(h_mi, c_x)

        if DEBUG:
            print("sc_1", sc_1.size())
            print("sc_2", sc_2.size())

        if s_bias1 is not None:
            sc_1 += s_bias1
        if s_bias2 is not None:
            sc_2 += s_bias2

        logits = torch.cat((sc_1, sc_2), 1)

        return logits


class DGI(nn.Module):
    def __init__(self, n_in, n_h, activation=None, gcn=None):
        super(DGI, self).__init__()
        # Only 1 layer for now
        if gcn is not None:
            self.gcn = gcn
        else:
            self.gcn = GraphConv(n_in, n_h, activation=activation)
        self.read = AvgReadout()
        self.sigm = nn.Sigmoid()
        self.disc = Discriminator(n_h)

    def forward(self,
                seq1, # Features true
                seq2, # Features corrupted
                graph, # can be nodeflow
                msk=None,
                samp_bias1=None,
                samp_bias2=None,
                prefix1=None,
                prefix2=None):
        """ Encode and discriminate resulting representation """
        if prefix1 is not None:
            h_1 = self.gcn(seq1, graph, prefix=prefix1)
        else:
            h_1 = self.gcn(seq1, graph)

        c = self.read(h_1, msk)
        c = self.sigm(c)

        if prefix2 is not None:
            h_2 = self.gcn(seq2, graph, prefix=prefix2)
        else:
            h_2 = self.gcn(seq2, graph)

        ret = self.disc(c, h_1, h_2, samp_bias1, samp_bias2)

        return ret

    # Detach the return variables
    def embed(self, seq, graph, node_ids=None, return_summary=False):
        """ Embed inputs into representation space, detach. """
        h_1 = self.gcn(seq, graph)

        if node_ids is None:
            # Return h_1 for all nodes
            if return_summary:
                return h_1.detach(), self.read(h_1, None)
            else:
                return h_1.detach()


        # Return h_1 only for node_ids
        if return_summary:
            # Get masks from node_ids
            msk = torch.zeros(graph.number_of_nodes())
            msk[node_ids] = 1.0
            c = self.read(h_1, msk)
            return h_1.detach()[node_ids], c.detach()
        else:
            return h_1.detach()[node_ids]



