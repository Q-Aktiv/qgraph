"""
File: utils.py
Author: Lukas Galke
Email: python@lpag.de
Github: https://github.com/lgalke
Description: Utilities for the qgraph package
"""
import os
import csv
import pickle
from tqdm import tqdm
import itertools as it

import numpy as np
import pandas as pd

from sklearn.metrics import pairwise_distances_chunked

from .preprocessing import normalize_text


def save_embedding(labels, embedding, outfile):
    """ Saves an embedding given by (unique) labels and vectors to outfile """
    df = pd.DataFrame(data=embedding, index=labels)
    df.to_csv(outfile, index=True, header=False)


def load_embedding(csvfile, as_dataframe=False):
    """ Loads an embedding, either as array + labels or as dataframe """
    df = pd.read_csv(csvfile, header=None, index_col=0)
    if as_dataframe:
        return df
    return df.index.values, df.values


def filter_by_top_counts(dataframe, col, keep):
    if isinstance(keep, float):
        assert keep > 0. and keep < 1.
        n_distinct = dataframe[col].nunique()
        keep = int(keep * n_distinct)
        print("Keeping {} of {} distinct {} values".format(keep, n_distinct, col))
    assert isinstance(keep, int), "Put int or float to make this work"
    top_items = pd.value_counts(dataframe[col], sort=True, ascending=False).index[:keep]
    return dataframe[dataframe[col].isin(top_items)]


def pfeffer_eq(title_a, title_b, authors=None):
    """ Pfeffer equality, title equals with special chars removed
    If `authors` is given, it is expected a pair of author lists ([str],[str])
    """
    if normalize_text(title_a) == normalize_text(title_b):
        # If titles are equal, check authors
        if authors:
            authors_a, authors_b = authors
            if isinstance(authors_a, str) or isinstance(authors_b, str):
                # This would check for joint *characters* in the authors, thus raises ValueError
                raise ValueError("Authors argument must be pair of *list of* strings if given")
            cutset = set(authors_a) & set(authors_b)
            return bool(cutset)

        # Titles are equal and no authors given
        return True

    # Titles not equal
    return False


# def save_embedding(path, labels, vectors):
#     """ Saves an embedding as pickled dict.
#     labels[i] should correspond to vectors[i]
#     """
#     embedding = {
#         'labels': list(labels),
#         'vectors': np.asarray(vectors)
#     }
#     with open(path, 'wb') as file:
#         pickle.dump(embedding, file)


# def load_embedding(path):
#     with open(path, 'rb') as file:
#         embedding = pickle.load(file)
#     return embedding['labels'], embedding['vectors']



def dump_pairwise_distances(outfile,
                            X, Y=None,
                            metric='euclidean',
                            n_jobs=4,
                            index2label=None):
    """
    Dumps pairwise distances to disk

    :outfile: The file to dump the pairwise distances
    :X: Node representations (for instance: class prototypes)
    :Y: (Optional) other node representations
    :n_jobs: How many threads to use
    :index2label: a mapping to resolve the label indices

    """
    print("Dumping pairwise distances to", outfile)
    # Generate a distance matrix chunk by chunk
    gen = pairwise_distances_chunked(X, Y,
                                     metric=metric,
                                     n_jobs=n_jobs)

    with open(outfile, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["class1", "class2", "distance"])  # Header row
        start = 0
        for d_chunk in tqdm(gen):
            for row, col in it.product(range(d_chunk.shape[0]),
                                       range(d_chunk.shape[1])):
                triple = (index2label[start+row],
                          index2label[col],
                          d_chunk[row, col])
                print(triple)
                writer.writerow(triple)
            start += d_chunk.shape[0]



def dump_temporal_embeddings(outdir,
                             embed_fn,
                             dgl_graph, features,
                             paper_node_ids,
                             paper_years,
                             concept_node_ids,
                             concept_descriptors,
                             min_year=None,
                             ccount_path=None):
    """
    Dump temporal embeddings to `outdir`
    embed_fn: Function[features, g] -> node_embedding
    """
    paper_node_ids = np.asarray(paper_node_ids)
    paper_years = np.asarray(paper_years)

    has_valid_year = ~np.isnan(paper_years)
    print("%d of %d papers have a valid year" % (has_valid_year.sum(), paper_years.shape[0]))
    paper_node_ids, paper_years = paper_node_ids[has_valid_year], paper_years[has_valid_year]

    if min_year is not None:
        min_year = int(min_year)
        print("Using custom min year: ", min_year)
    else:
        min_year = int(paper_years.min())
        print("Infering min year from data:", min_year)

    all_years = np.unique(paper_years)  # Unique sorts
    all_years = all_years[all_years >= min_year]

    print("All years:", all_years)

    # Attach features to node data...
    dgl_graph.ndata['features'] = features


    print("Dumping embeddings between %d and %d to %s" % (all_years[0], all_years[-1], outdir))
    os.makedirs(outdir, exist_ok=True)

    cumsums = []
    distinct_concepts = []


    print("Processing annual papers between {} and {}".format(all_years[0], all_years[-1])) 
    for year in tqdm(all_years):
    # for paper-haufen in paper-haufen-generator( )
        paper_nids_until = paper_node_ids[paper_years <= year]

        # Remember how many papers there until year
        cumsums.append(len(paper_nids_until))

        # Induce subgraph on concept nodes and paper nodes
        # node list should be sorted
        nodes = sorted(list(concept_node_ids) + list(paper_nids_until))
        subg = dgl_graph.subgraph(nodes)  # DGL

        # Copy features from parent graph...
        subg.copy_from_parent()

        subg_concept_nids = subg.map_to_subgraph_nid(concept_node_ids)

        # Compute indegrees of concepts nodes
        # and count distinct concepts if in-degree is > 0
        concept_count = (subg.in_degrees(subg_concept_nids) > 0).sum()
        distinct_concepts.append(concept_count.item())

        features = subg.ndata.pop('features')
        embedding = embed_fn(features, subg, subg_concept_nids).cpu().numpy()

        outfile_year = os.path.join(outdir, str(int(year)) + '.csv')
        save_embedding(concept_descriptors, embedding, outfile_year)

    if ccount_path:
        print("Printing year stats to", ccount_path)
        with open(ccount_path, 'w') as fh:
            print("year","papers_cum","distinct_concepts", file=fh, sep=',')
            for year, cumsum, ccount in zip(all_years, cumsums, distinct_concepts):
                print(int(year), cumsum, int(ccount), file=fh, sep=',')

    # Clean up, restore orig state
    dgl_graph.ndata.pop('features')

if __name__ == "__main__":
    import doctest
    doctest.testmod()
