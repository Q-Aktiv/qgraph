import random

import networkx as nx

import matplotlib.pyplot as plt
import pyveplot as pv


def draw_graph(G, file):
    """ Draw basic graph """
    plt.subplot(121)
    nx.draw(G)
    plt.subplot(122)
    nx.draw(G, pos=nx.circular_layout(G), nodecolor='r', edge_color='b')
    plt.savefig(file)


def plot_hive(G, file):
    """ See also: https://pypi.org/project/pyveplot/ """
    h = pv.Hiveplot(file)
    axis0 = pv.Axis( (200,200), (200,100), stroke="grey")
    axis1 = pv.Axis( (200,200), (300,300), stroke="blue")
    axis2 = pv.Axis( (200,200), (10,310),  stroke="black")
    h.axes = [ axis0, axis1, axis2 ]

    axis_from_prefix = {
        '__AUTHOR__': axis0,
        '__PAPER__': axis1,
        '__KEYWORD__': axis2,
    }

    for n in G.nodes():
        node = pv.Node(n)
        axis_from_prefix[n.split()[0]].add_node(node, random.random())

    for e in G.edges():
        if (e[0] in axis0.nodes) and (e[1] in axis1.nodes):       # edges from axis0 to axis1
            h.connect(axis0, e[0], 45,
                      axis1, e[1], -45,
                      stroke_width='0.34', stroke_opacity='0.4',
                      stroke='purple')
        elif (e[0] in axis0.nodes) and (e[1] in axis2.nodes):     # edges from axis0 to axis2
            h.connect(axis0, e[0], -45,
                      axis2, e[1], 45,
                      stroke_width='0.34', stroke_opacity='0.4',
                      stroke='red')
        elif (e[0] in axis1.nodes) and (e[1] in axis2.nodes):     # edges from axis1 to axis2
            h.connect(axis1, e[0], 15,
                      axis2, e[1], -15,
                      stroke_width='0.34', stroke_opacity='0.4',
                      stroke='magenta')
        
    h.save()



