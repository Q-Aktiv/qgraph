"""
File: layers.py
Author: Lukas Galke
Email: vim@lpag.de
Github: https://github.com/lgalke
Description: Layers for building graph NNs
"""
import torch
import torch.nn as nn
import torch.nn.functional as F


class InnerProductDecoder(nn.Module):

    """Given inputs: [batch,hidden], this module with compute pairwise inner products
    of shape [batch,batch]. Go-to decoder for link-prediction.
    """

    def __init__(self):
        nn.Module.__init__(self)

    def forward(self, inputs):
        x = inputs
        a_hat = x @ x.t()
        return a_hat


        

