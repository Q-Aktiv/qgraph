import torch
import torch.nn as nn
import torch.nn.functional as F

def compute_gcn_norm(g):
    """compute_gcn_norm
    Normalizes a dglgraph according to paper.
    Self-loops should be added in advance!

    :param g: DGLGraph
    """
    # normalization
    degs = g.in_degrees().float()
    norm = torch.pow(degs, -0.5)
    norm[torch.isinf(norm)] = 0
    return norm.unsqueeze(1)
