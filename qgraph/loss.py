# Regularization loss from GCN paper
import torch
import torch.nn as nn

class GCNRegularizationLoss(nn.Module):

    """Regularization loss from GCN paper"""

    def __init__(self, g):
        """Initializes the (stateful) loss

        :g:

        """
        nn.Module.__init__(self)

        adj = g.adjacency_matrix()
        deg = g.in_degrees()  # Symmetric, in_degrees == out_degrees
        self._delta = torch.diag(deg) - adj

    def forward(self, inputs):
        # return inputs.t() @ self._delta @ inputs
        # Maybe more efficient!
        return torch.chain_matmul(inputs.t(), self._delta, inputs)



class NegativeSamplingLoss(nn.Module):

    """Negative sampling loss"""

    def __init__(self, num_classes, embed_size, weights=None):
        """Inits the loss

        :num_classes: TODO
        :embed_size: TODO
        :weights: TODO

        """
        nn.Module.__init__(self)

        self.num_classes = num_classes
        self.embed_size = embed_size
        self.weights = weights
        if self.weights is not None:
            assert min(self.weights) >= 0, "Each weight should be >= 0"

            self.weights = Variable(t.from_numpy(weights)).float()
        self.out_embed = nn.Embedding(self.num_classes, self.embed_size, sparse=True)
        self.out_embed.weight = Parameter(torch.FloatTensor(self.num_classes, self.embed_size).uniform_(-1, 1))

    def sample(self, num_sample):
        """
        draws a sample from classes based on weights
        """
        return torch.multinomial(self.weights, num_sample, True)

    def forward(self, input_representation, out_labels, num_sampled):
        use_cuda = self.out_embed.weight.is_cuda
        batch_size, window_size = out_labels.size()

