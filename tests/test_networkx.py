import networkx as nx



def test_adjacency_matrix():

    nodes = list("abcdefg")

    edges = [
        ('a', 'b'),
        ('a', 'c'),
        ('a', 'd'),
        ('b', 'e'),
        ('b', 'a')]

    g = nx.DiGraph()

    g.add_nodes_from(nodes)
    g.add_edges_from(edges)

    adj = nx.to_scipy_sparse_matrix(g, nodelist=nodes)

    assert not adj[0, 0]
    assert adj[0, 1]
    assert adj[0, 2]
    assert adj[0, 3]
    assert adj[1, 4]
    assert not adj[1, 5]
    assert adj[1, 0]
    # Directed graph, reverse edge should not be in
    assert not adj[2, 0]

def test_adjacency_matrix_undirected():

    nodes = list("abcdefg")

    edges = [
        ('a', 'b'),
        ('a', 'c'),
        ('a', 'd'),
        ('b', 'e'),
        ('b', 'a')]

    # Undirected graph
    g = nx.Graph()

    g.add_nodes_from(nodes)
    g.add_edges_from(edges)

    adj = nx.to_scipy_sparse_matrix(g, nodelist=nodes)

    assert not adj[0, 0]
    assert adj[0, 1]
    assert adj[0, 2]
    assert adj[0, 3]
    assert adj[1, 4]
    assert not adj[1, 5]
    assert adj[1, 0]
    # Undirected graph, reverse edge should be in
    assert adj[1, 0]
    assert adj[2, 0]
    assert adj[3, 0]
    assert adj[4, 1]
    assert adj[0, 1]
