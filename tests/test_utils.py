""" Tests for qgraph/utils.py
"""

import pytest
import numpy

from qgraph.utils import pfeffer_eq
from qgraph.utils import save_embedding, load_embedding




def test_pfeffer_eq():
    """ Test pfeffer equality function """
    assert pfeffer_eq("simple", "simple")
    assert pfeffer_eq("this is great", "THIS IS GREAT!")
    assert not pfeffer_eq("this is not great", "this is great!")
    assert pfeffer_eq("Wait: what's wrong?", "Wait: what's wrong?")
    assert not pfeffer_eq("Wait: what's wrong?", "Wait: what's wrong?",
                          authors=(["hans"], ["peter"]))
    assert pfeffer_eq("Wait: what's wrong?", "Wait: what's wrong?",
                      authors=(["hans"], ["hans"]))
    assert pfeffer_eq("Wait: what's wrong?", "Wait: what's wrong?",
                      authors=(["a1", "hans"], ["hans", "a2"]))
    with pytest.raises(ValueError):
        # This would check for joint *characters* in the authors, thus it should raise ValueError
        pfeffer_eq("Cookie", "Cookie", authors=("Bahlsen", "Oreo"))
    with pytest.raises(ValueError):
        # This would check for joint *characters* in the authors, thus it should raise ValueError
        pfeffer_eq("Cookie", "Cookie", authors=(["Bahlsen"], "Oreo"))
    assert not pfeffer_eq("Cookie", "Cookie", authors=(["Bahlsen"], ["Oreo"]))


def test_embedding_saveload():
    labels1 = ["the", "quick", "brown", "fox"]
    vectors1 = numpy.random.rand(4, 10)

    tmppath = 'tmp-embedding.csv'
    eps = 1e-8

    save_embedding(labels1, vectors1, tmppath)

    labels2, vectors2 = load_embedding(tmppath)

    for i in range(len(labels1)):
        assert labels2[i] == labels1[i]

    diff = vectors2 - vectors1

    assert (diff < eps).all()

