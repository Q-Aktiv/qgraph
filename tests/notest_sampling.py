import dgl
import sys

from dgl.contrib.sampling.sampler import NeighborSampler


# def next(sth):
#     # hack a next 
#     for x in sth:
#         return x


def test_add_self_loop():
    """ test sampler with self loop """
    g = dgl.DGLGraph()

    g.add_nodes(10)
    g.add_edges([0,1,4,5],[4,5,8,9])

    g.readonly()
    print(g)

    sampler = NeighborSampler(g, 2, 100, num_hops=2, seed_nodes=[8,9], add_self_loop=True) 


    print(sampler, file=sys.stderr)

    print(vars(sampler), file=sys.stderr)
    nf = next(sampler)

    assert nf.layer_size(0) == 6
    assert nf.layer_size(1) == 4
    assert nf.layer_size(2) == 2



def test_no_self_loop():
    """ test sampler without self loop """
    g = dgl.DGLGraph()

    g.add_nodes(10)
    g.add_edges([0,1,4,5],[4,5,8,9])

    g.readonly()
    print(g)

    sampler = NeighborSampler(g, 2, 100, num_hops=2, seed_nodes=[8,9], add_self_loop=False) 


    nf = next(sampler)

    assert nf.layer_size(0) == 2
    assert nf.layer_size(1) == 2
    assert nf.layer_size(2) == 2



def test_given_self_loop():
    g = dgl.DGLGraph()

    g.add_nodes(10)
    g.add_edges([0,1,4,5],[4,5,8,9])
    g.add_edges(g.nodes(), g.nodes())

    g.readonly()
    print(g)

    sampler = NeighborSampler(g, 2, 100, num_hops=2, seed_nodes=[8,9], add_self_loop=False) 


    nf = next(sampler)

    # Note that self loops are added when they are given in the graph
    # even though add_self_loop is set to False in the sampling procedure
    assert nf.layer_size(0) == 6
    assert nf.layer_size(1) == 4
    assert nf.layer_size(2) == 2


def test_given_and_add_self_loop():
    g = dgl.DGLGraph()

    g.add_nodes(10)
    g.add_edges([0,1,4,5],[4,5,8,9])
    g.add_edges(g.nodes(), g.nodes())

    g.readonly()
    print(g)

    sampler = NeighborSampler(g, 2, 100, num_hops=2, seed_nodes=[8,9], add_self_loop=True) 


    nf = next(sampler)

    # Note that self loops are added when they are given in the graph
    # even though add_self_loop is set to False in the sampling procedure
    assert nf.layer_size(0) == 6
    assert nf.layer_size(1) == 4
    assert nf.layer_size(2) == 2
