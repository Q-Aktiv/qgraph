"""
File: svd.py
Author: Lukas Galke
Email: vim@lpag.de
Github: https://github.com/lgalke
Description: Content-based analysis via SVD
"""

import argparse
import json
import csv
from tqdm import tqdm
from itertools import combinations, product

import numpy as np
import pandas as pd

import sklearn

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import MultiLabelBinarizer, normalize
from sklearn.decomposition import TruncatedSVD

from sklearn.metrics import pairwise_distances_chunked
from sklearn.metrics.pairwise import PAIRWISE_DISTANCE_FUNCTIONS


def class_prototypes(x_docs, y_labels):
    """
    Compute a prototype for each label, composed as the average of its
    document's representation.
    :x_docs: array-like or sparse matrix of shape (n_docs, n_features)
    :y_labels: array-like or sparse matrix of shape (n_docs, n_classes)
    :returns: class prototypes of shape (n_classes, n_features)
    """
    return normalize(y_labels.T, norm='l1') @ x_docs


def most_frequent_labels(y_labels, n):
    """ Returns the indices of the most-assigned labels """
    n_assignments = y_labels.sum(0)
    ascending_indices = n_assignments.argsort().A1
    most_frequent = ascending_indices[-n:]
    return most_frequent


def has_label(labels, col):
    """ Returns indices of rows that are annotated with a certain label """
    return labels[:, col].nonzero()[0]

def main():
    """ Run LSA on bibliographic metadata

    Required format: {articles:[{abstractText:str, title:str, meshMajor:str}]}
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('data', type=argparse.FileType('r', errors='ignore'),
                        help="Path to json file")
    parser.add_argument('-n', '--n-components', type=int, default=100,
                        help="Number of components for SVD")
    parser.add_argument('-j', '--n-jobs', type=int, default=8,
                        help="Number of jobs for computing distances.")
    parser.add_argument('-o', '--outfile', type=str, required=True,
                        default=None, help="File to store pairwise distances")
    parser.add_argument('-m', '--metric', type=str,
                        choices=PAIRWISE_DISTANCE_FUNCTIONS,
                        default='euclidean',
                        help="Metric for pairwise distances ['euclidean']")
    parser.add_argument('-f', '--field', type=str,
                        choices=('title', 'abstractText'),
                        default='title')
    parser.add_argument('-d', '--dialect', type=str,
                        choices=csv.list_dialects(),
                        default='excel-tab',
                        help="Output csv dialect")
    parser.add_argument('-M', '--memory',
                        default=100*1024, type=int,
                        help="Memory limit in MB [100 * 1024]")
    parser.add_argument('--only-stats', action='store_true',
                        default=False,
                        help="Compute collections statistics and exit")
    args = parser.parse_args()

    print(args)

    # used by `pairwise_distances_chunked`
    sklearn.set_config('working_memory', args.memory)

    print("Loading json file:", args.data)
    data = json.load(args.data)
    args.data.close()

    print("Putting stuff into a dataframe")
    df = pd.DataFrame.from_records(data['articles'], index=['pmid'])
    print("#Records:", len(df))
    assert args.field in df, "Field '{}' not in data".format(args.field)

    print("Transforming field", args.field)
    tfidf = TfidfVectorizer(stop_words='english', max_features=50000)
    x_tfidf = tfidf.fit_transform(df[args.field].values)
    print("Shape of Term-Document matrix:", x_tfidf.shape)

    print("Transforming mesh labels")
    mlb = MultiLabelBinarizer(sparse_output=True)
    y_mesh = mlb.fit_transform(df['meshMajor'].values)
    print("Found", len(mlb.classes_), "classes")

    print("Classes:", mlb.classes_[:10], "...", mlb.classes_[-10:], sep='\n')

    if args.only_stats:
        exit(0)

    print("Fitting SVD with {} components".format(args.n_components))
    svd = TruncatedSVD(n_components=args.n_components)
    x_svd = svd.fit_transform(x_tfidf)
    print("Transformed document representation shape:", x_svd.shape)

    print("Explained variance {:.2%}".format(svd.explained_variance_ratio_.sum()))

    print("Computing class prototypes")
    prototypes = class_prototypes(x_svd, y_mesh)

    print("Computing pairwise class distances")
    # Generate a distance matrix chunk by chunk
    gen = pairwise_distances_chunked(prototypes,
                                     metric=args.metric,
                                     n_jobs=args.n_jobs)

    with open(args.outfile, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect=args.dialect)
        writer.writerow(["class1", "class2", "distance"])  # Header row
        start = 0
        for d_chunk in tqdm(gen):
            for row, col in product(range(d_chunk.shape[0]),
                                    range(d_chunk.shape[1])):
                triple = (mlb.classes_[start+row],
                          mlb.classes_[col],
                          d_chunk[row, col])
                print(triple)
                writer.writerow(triple)
            start += d_chunk.shape[0]


if __name__ == "__main__":
    main()
