#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Executable to compute LSA prototypes for pubmed data """

import argparse
import json

import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer

from qgraph.embeddings import LSAEmbedding, class_prototypes
from qgraph.utils import save_embedding

def main(args):
    """ Run the executable """

    print("Loading json file:", args.jsonfile)
    with open(args.jsonfile, errors='ignore') as datasource:
        data = json.load(datasource)

    df = pd.DataFrame.from_records(data['articles'], index=['pmid'])

    print("#Records:", len(df))
    if args.year is not None:
        print("Selecting only papers until year", args.year)
        df['year'] = df['year'].to_numeric()
        df = df[df['year'] <= args.year]
        print("#Records up to year {}:".format(args.year), len(df))

    assert args.field in df, "Field '{}' not in data".format(args.field)

    print("Transforming mesh labels")
    mlb = MultiLabelBinarizer(sparse_output=True)
    y_mesh = mlb.fit_transform(df['meshMajor'].values)
    print("Found", len(mlb.classes_), "classes")

    print("Running LSA on field '{}'".format(args.field))
    X = df[args.field].values
    lsa = LSAEmbedding(n_components=args.n_components,
                       max_features=args.limit_vocab,
                       stop_words=('english' if args.stop_words else None))

    X_lsa = lsa.fit_transform(X)

    print("Computing prototypes")
    prototypes = class_prototypes(X_lsa, y_mesh)

    print("Saving prototypes to", args.outfile)
    save_embedding(args.outfile, mlb.classes_, prototypes)


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument('jsonfile', help="Path to json file")
    PARSER.add_argument('-y', '--year', default=None, type=int,
                        help="Consider documents up to this year.")
    PARSER.add_argument('-l', '--limit-vocab', default=None, type=int,
                        help="Limit vocabulary size to frequent words")
    PARSER.add_argument('-n', '--n-components', default=100, type=int,
                        help="Number of components for SVD")
    PARSER.add_argument('-S', '--dont-stop', dest='stop_words',
                        default=True, action='store_false',
                        help="Do not use english stop words.")
    PARSER.add_argument('-f', '--field', type=str,
                        choices=('title', 'abstractText'),
                        default='title')
    # PARSER.add_argument('-d', '--dialect', type=str,
    #                     choices=csv.list_dialects(),
    #                     default='excel-tab',
    #                     help="Output csv dialect")
    PARSER.add_argument('-o', '--outfile', type=str, required=True,
                        default=None, help="File to store pairwise distances")

    ARGS = PARSER.parse_args()
    main(ARGS)
